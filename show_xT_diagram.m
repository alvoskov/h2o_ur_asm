% SHOW_XT_DIAGRAM Show the diagram in x-T diagram.
% Calculated diagram is shown with lines, experimental data
% are shown as dots (i.e. markers)
%
% Usage:
%   show_xT_diagram(data, a, stab_param, a_to_g)
% Inputs:
%   data -- struct array with x, T and marker fields
%           (data series to be shown 
%   a    -- thermodynamic model parameter
% See also CALC_LIQUIDUS_TV, TD_MODEL

% Programming: Alexey Voskov
% License: MIT
function show_xT_diagram(data, a, stab_param, a_to_g)
xl_calc = 0:0.001:1;
[Tl_calc, x_eut_calc, T_eut_calc] = calc_liquidus_Tv(xl_calc, a, stab_param, a_to_g);
[~,~,G,~,~,Gex] = td_model(xl_calc, 298.15, a, a_to_g);
rel = Gex./G; rel(isnan(rel)) = [];
fprintf('Average percentage of exceess G: %.5f\n', mean(abs(rel)));
fprintf('Eutectic point: x=%.5f; T=%.5f\n', x_eut_calc, T_eut_calc);
plot(xl_calc, Tl_calc, 'k-', ...
    [0 1], [T_eut_calc T_eut_calc], 'k-', 'LineWidth', 2);
hold on;
for ser = data
    plot(ser.x, ser.T, ser.marker, 'LineWidth', 2);
end
hold off;
xlabel(sprintf('$x_\\mathrm{%s}$', stab_param.comp2), 'FontSize', 14, 'Interpreter', 'latex');
ylabel('$T$ / K', 'FontSize', 14, 'Interpreter', 'latex');
%xlabel(sprintf('{\\it{x}}_{%s}', stab_param.comp2), 'FontSize', 14);
%ylabel('{\it{T}} / K', 'FontSize', 14);
set(gcf, 'Units', 'Centimeters');
set(gcf, 'Position', [0 0 10 8]);
set(gcf, 'PaperPositionMode', 'auto');
set(gca, 'FontSize', 12);
% Set 20-40 K step on the Y axis
YTicks = get(gca, 'YTick');
if YTicks(2) - YTicks(1) >= 20
    dT = 40;
else
    dT = 20;
end
YTicks_new = YTicks(1):dT:(YTicks(end) + 2*eps);
YTickLabels_new = cell(size(YTicks_new));
for i = 1:numel(YTicks_new)
    YTickLabels_new{i} = num2str(YTicks_new(i));
end
a = axis;
if abs(a(4) - YTicks_new(end)) > eps
    YTicks_new(end+1) = YTicks_new(end) + dT;
    YTickLabels_new{end+1} = num2str(YTicks_new(end));
end
a(4) = YTicks_new(end);
axis(a);

set(gca, 'YTick', YTicks_new);
set(gca, 'YTickLabel', YTickLabels_new);
end
