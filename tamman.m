% TAMMAN  Constructs Tamman triangles using experimental data about
% UR-ASM and H2O-ASM binary subsystem
%
% Usage:
%   tamman(sys, save_fig)
% Inputs:
%   sys -- 'ur_asm' (default) or 'w_asm' (case-sensitive)
%   save_fig -- true (default) or (false) -- save/don't save figures
%               into files
%
%   All input arguments are optional
%
% See also H2O_ASM, UR_ASM, H2O_UR

% Programming: Alexey Voskov
% License: MIT
function tamman(sys, save_fig)

% Set default arguments if required
if nargin < 2
    save_fig = true;
end
if nargin < 1
    sys = 'ur_asm';
end
% Components properites
H2Oprop = compProp('W');
URprop = compProp('UR');
ASMprop = compProp('ASM');
% Default 
switch sys
    case 'ur_asm'
    %Table 3. Solidus melting peak area (s) of Ur (2) – ASM (3) samples a 
    M1 = URprop.M; % Ur
    M2 = ASMprop.M; % ASM
    ur_asm_dat = [ % wASM, s
    0       0       
    2.01	16.3	
    5.09	35.5	
    10.08	74.6	
    15.01	117.5	
    19.90	142.8	
    29.87	213.8	
    40.22	307.1	
    44.92	333.5	
    59.48	361.2	
    64.87	314.3
    69.86	280.2	
    80.00	180.3
    84.61	130.3
    89.94	87.7
    95.18	40.5
    100     0];
    w = ur_asm_dat(:,1);
    s = ur_asm_dat(:,2);
    ii = w < 50;
    filename = 'fig3_ur_asm_tamman';
    
    case 'w_asm'
    %Table 5. Solidus melting peak area (s) of W (1) – ASM (3) samples.
    M1 = H2Oprop.M; % H2O
    M2 = ASMprop.M; % ASM
    w_asm_dat = [ % wASM, s
    0       0       
    10.59	90.6	
    15.31	134.0	
    19.82	177.6		
    20.71	183.8	
    25.94	226.0	
    25.91	226.5
    30.61	270.5	
    50.09	294.7
    55.24	254.5	
    61.16	225.1
    66.12	193.6
    75.92	132.4
    100     0
    ];
    w = w_asm_dat(:,1);
    s = w_asm_dat(:,2);
    ii = w < 40;
    filename = 'figX_ur_asm_tamman';
    
    otherwise
    error('Unknown system %s; use ur_asm or w_asm', sys);
end
% Divide data into two subsets (for linear regression)
w1 = w(ii); w2 = w(~ii);
s1 = s(ii); s2 = s(~ii);
% Linear regression for each subset (with calculation of conf.int.)
[ab1, dab1] = linreg_2d(w1, s1)
[ab2, dab2] = linreg_2d(w2, s2)
% Mass fraction of ASM in the eutectic point (with conf.int.)
wx = -(ab1(2) - ab2(2))./(ab1(1) - ab2(1));
dwx1 = sqrt(dab1(2).^2 + dab2(2).^2); ewx1 = dwx1 ./ abs(ab1(2) - ab2(2));
dwx2 = sqrt(dab1(1).^2 + dab2(1).^2); ewx2 = dwx2 ./ abs(ab1(1) - ab2(1));
ewx = sqrt(ewx1.^2 + ewx2.^2);
dwx = ewx .* wx;
% Molar fraction of ASM in the eutectic point (with conf.int.)
xx = wx ./ (wx + (100 - wx)*M2/M1);
dxx1 = dwx; dxx2 = sqrt(dwx.^2 + dwx.^2*M2/M1)
exx1 = ewx; exx2 = dxx2 ./ (wx + (100 - wx)*M2/M1);
exx = sqrt(exx1.^2 + exx2.^2);
dxx = exx * xx;
% Show the result in the text form
fprintf('wx = (%.3f)+-(%.3f)\n', wx, dwx);
fprintf('xx = (%.5f)+-(%.5f)\n', xx, dxx);
% Show the diagram
close all;
plot([0 wx+1], ab1(1)*[0 wx+1] + ab1(2), 'k-', ... % Regression 1
    [wx-1 100], ab2(1)*[wx-1 100] + ab2(2), 'k-', ... % Regression 2
    w, s, 'bo', 'LineWidth', 2); % Experimental data + diag.settings
set(gca, 'FontSize', 12);
xlabel('{\it{w}}_{NH_4SO_3NH_2}, %', 'FontSize', 14);
ylabel('{\it{s}} / mVs\cdot{mg}^{-1}', 'FontSize', 14);
set(gcf, 'Units', 'Centimeters');
set(gcf, 'Position', [0 0 10 8]);
p = get(gca, 'Position');
p(1) = p(1) + 0.01; p(3) = p(3) - 0.01; % To prevent y axis title cropping
set(gca, 'Position', p);
set(gcf, 'PaperPositionMode', 'auto');
axis([0 100 0 425]);
set(gca, 'YTick', [0 100 200 300 400]);
% Save the diagram to the file
if save_fig
    save_fig_to_files('Saving the diagram to files...', filename);
end
end

function [ab, dab] = linreg_2d(x, y)
% y = ax + b regression
x = x(:); y = y(:);
X = [x ones(size(x))];
ab = X \ y;
sigma2 = sumsqr(ab(1) * x + ab(2) - y) / (numel(x) - 2);
t = tinv(1 - 0.05/2, numel(x) - 2);
sab = sqrt(diag(sigma2*inv(X'*X)));
dab = sab * t;
end
