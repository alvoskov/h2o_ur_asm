% COMPPROP  Returns component properties in the form of the structure
% for the Water - Urea - Ammonium Sulfamate ternary system
%
% Input: 
%  id -- name of the component ('W', 'UR', 'ASM')
%
% Output: structure with the next fields
%  dmH  -- enthalpy of melting, J/mol
%  Tm   -- temperature of melting, K
%  dmCp -- change of heat capacity during melting, J/(mol*K)
%  dmG_func -- @(T) (...) function handle for Gibbs energy of melting
%  dmH_func -- @(T) (...) function handle for enthalpy of melting
%
% See also: GETR

% Programming: Alexey Voskov
% License: MIT
function prop = compProp(id)
if ~ischar(id)
    error('id must be a string');
end
% Create struct with properties values
switch id
    case 'W' % Water thermodynamic properties
        prop = struct(...
            'M', 18.0153, ... % M(H2O), g/mol
            'dmH', 6010, ...
            'Tm', 273.15, ...
            'dmCp', 38.21);
    case 'UR' % Urea thermodynamic properties
        prop = struct(...
            'M', 60.0553, ... % M((NH2)2CO), g/mol
            'dmH', 14644, ...
            'Tm', 405.85, ...            
            'dmCp', 33.95);
    case 'ASM'
        prop = struct(...
            'M', 114.124, ... % M(ASM), g/mol
            'dmH', 16700, ...
            'Tm', 406.1, ...
            'dmCp', 0);
    otherwise
        error('Unknown component %s', id);
end
% Generate functions handles
prop.dmG_func = @(T) dmG_func(T, prop.dmH, prop.Tm, prop.dmCp);
prop.dmH_func = @(T) dmH_func(T, prop.dmH, prop.Tm, prop.dmCp);
end

function G = dmG_func(T, dmH, Tm, dmCp)
G = (dmH + dmCp*(T - Tm)) - T.*(dmH/Tm + dmCp*log(T/Tm));
end

function H = dmH_func(T, dmH, Tm, dmCp)
H = dmH + dmCp*(T - Tm);
end
