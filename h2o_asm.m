% H2O_ASM  H2O(1)-ASM(2) binary system thermodynamic modeling
%
% See also compProp

% Programming: Alexey Voskov
% License: MIT
function h2o_asm(save_fig)
if nargin == 0
    save_fig = true;
end
close all;
% ---- Load components properties
H2Oprop = compProp('W');
ASMprop = compProp('ASM');
Mw = H2Oprop.M;
dmG_ASM = ASMprop.dmG_func;
dmG_H2O = H2Oprop.dmG_func;
% ---- beginning of experimental data
% Solidus: our experimental data only
Ts = [-18.3 -18.4 -18.3 -18.3 -18.3 -18.3 -18.3 -18.3...
    -18.4  -18.3  -18.3  -18.4]' + 273.15;
xs = [0.0087 0.0184 0.0278 0.0396 0.0524 0.0651 0.094 0.094...
    0.1368 0.1631 0.1991 0.2356]';
txl = [
-0.9	0.0087  1   % Our experimental data
-2.0	0.0184  1
-4.3	0.0278  1
-5.4	0.0396  1
-8.2	0.0524  1
-10.1	0.0651  1
%-18.3	0.094   1
%-18.3	0.094   1
0	    0.1766  2   % Ushida
9	    0.2067  3   % Ricci51
20	    0.2428  3   % Ricci47
25	    0.2592  3   % Ricci47
30	    0.2702  2   % Ushida
35	    0.2981  3   % Ricci47
45	    0.3400  3   % Ricci51
50	    0.3630  2   % Ushida
60	    0.4167  4   
70	    0.4551  2   % Ushida
105	    0.6418  4];
Tl = txl(:,1) + 273.15; xl = txl(:,2);
serinds = txl(:,3); % Data series indices
sernum = max(serinds);
Tl_ser = cell(1,sernum);
xl_ser = cell(1,sernum);
for i = 1:sernum
    Tl_ser{i} = Tl(serinds == i);
    xl_ser{i} = xl(serinds == i);
end

mpdp = [
0.79	3.10	0.03
1.96	3.00	0.03
5.07	2.78	0.03
9.14	2.51	0.03
12.58	2.32	0.02
16.85	2.13	0.02];
Tp = 25 + 273.15; % Older (and errorneous) variant: 25.1
mASM = mpdp(:,1); % mol/kg
xASM = mASM ./ (mASM + 1000/Mw);
pH2O = mpdp(:,2); dpH2O = mpdp(:,3); % kPa
% ----- Ending of experimental data

% Nonlinear regression: parameters mapping
a0 = [0 0 0 0];    
a_to_g = @(a, T) [a(1) + a(2)*T, a(3) + a(4)*T, 0];    
    
    % Nonlinear regression: target function    
    function delta = minfunc(a)
        p_calc = p_H2O(xASM, Tp, a, a_to_g);
        [Tl_c, x_eut_c, T_eut_c] = calc_liquidus_Tv(xl, a, stab_param, a_to_g);
        delta = [(Tl_c - Tl)./Tl; (p_calc - pH2O)./pH2O;...
            (T_eut_c - Ts)./Ts]*1000; %(x_eut_c - x_eut)./x_eut
    end

% Stability parameters
stab_param = struct('dmG1', dmG_H2O, 'dmG2', dmG_ASM, 'comp2', 'NH_4SO_3NH_2');
% Model parameters fit
a = td_model_fit(a0, @minfunc);
% Show the diagram (calculated vs experimental)
% a) big diagram
%data = struct('x',{xs,xl}, 'T', {Ts,Tl}, 'marker', {'rd', 'bo'});
data = struct('x',{xs,xl_ser{:}}, 'T', {Ts,Tl_ser{:}}, 'marker', {'ro', 'bo', 'bd', 'rs', 'bv'});
show_xT_diagram(data,a,stab_param,a_to_g);
% b) subplot
add_subplot = true;
if add_subplot
    axes('Position', [0.63, 0.28, 0.25, 0.3]);    
    xl_calc = 0:0.001:0.2;
    [Tl_calc, x_eut_calc, T_eut_calc] = calc_liquidus_Tv(xl_calc, a, stab_param, a_to_g);    
    hold on;
    plot([0 0.2], [T_eut_calc T_eut_calc], 'k-', 'LineWidth',2);
    plot(xl_calc, Tl_calc, 'k-', 'LineWidth', 2);
    plot(xs, Ts, 'ro', 'LineWidth', 2);
    plot(xl_ser{1}, Tl_ser{1}, 'bo', 'LineWidth', 2);
    hold off;
    box on;    
    axis([0 0.11 250 280]);
    set(gca, 'YTick', [250 260 270 280]);
    set(gca, 'YTickLabel', {'250', '260', '270', '280'});
end
% Save phase diagram to EPS or PDF file if required
if save_fig
    save_fig_to_files('Saving the phase diagram to files...', 'fig6_h2o_asm');
end    

% Show empirical eutectic temperature
fprintf('Teut(empirical) = %.2f\n', mean(Ts));
% Show the water pressure diagram
xv = 0:0.01:0.25;
figure;
plot(xv, p_H2O(xv,Tp,a,a_to_g), 'k-', xASM, pH2O, 'bo', 'LineWidth', 2);
xlabel('$x_\mathrm{NH_4SO_3NH_2}$', 'FontSize', 14, 'Interpreter', 'latex');
ylabel('$p$ / kPa', 'FontSize', 14, 'Interpreter', 'latex');
set(gca, 'FontSize', 12);
set(gcf, 'Units', 'Centimeters');
set(gcf, 'Position', [0 0 10 8]);
set(gcf, 'PaperPositionMode', 'auto');
if save_fig
    save_fig_to_files('Saving the pH2O figure to files...', 'fig7a_ph2o');
end
% Convexity control
for T = 230:5:500;
    xv = [1e-10, 0.005:0.005:0.995, 1 - 1e-10];
    [~,~,G,~,~,Gex] = td_model(xv, 298.15, a, a_to_g);
    k = unique(convhull(xv, G));
    if numel(k) == numel(G) 
        fprintf('T=%.2f K: convexity test PASSED\n', T)
    else
        fprintf('T=%.2f K: convexity test NOT PASSED\n', T);
    end
end
% Calculate branches of the phase diagram (for export in .csv file
% for Elsevier interactive plots)
fd = fopen('output/iplot_w_asm.csv', 'w');
fprintf(fd, 'x3(%%),T(K)\n');
Tv_floor = ceil(T_eut_calc);
% a) left branch
fprintf(fd, '%.3f,%.2f\n', [0; 273.15]);
Tv = 273:(-1):Tv_floor;
xv = calc_liquidus_xv(Tv,1,a,stab_param,a_to_g);
fprintf(fd, '%.3f,%.2f\n', [xv*100; Tv]);
% b) eutectic point
fprintf(fd, '%.3f,%.2f\n', x_eut_calc*100, T_eut_calc);
% c) right branch
Tv = sort([Tv_floor:1:406, (-10:5:130)+273.15]);
xv = calc_liquidus_xv(Tv,2,a,stab_param,a_to_g);
fprintf(fd, '%.3f,%.2f\n', [xv*100; Tv]);
fprintf(fd, '%.3f,%.2f\n', [100; 406.1]);
fclose(fd);

% Show the water pressure uncertainties
figure;
p_res = (pH2O - p_H2O(xASM,Tp,a,a_to_g))./pH2O;
plot(xASM, p_res*100, 'bo', 'LineWidth', 2);
xlabel('$x_\mathrm{NH_4SO_3NH_2}$', 'FontSize', 14, 'Interpreter', 'latex');
ylabel('$\left(p - p^\mathrm{calc}\right)/p^\mathrm{calc}$, \%', 'FontSize',14, 'Interpreter', 'latex');
p = axis;
p(3:4) = [-1 1];
axis(p);
hold on;
s = sqrt(sumsqr(p_res) ./ numel(p_res)) * 100;
plot(p(1:2), [0 0], 'k-', p(1:2), [-s -s], 'k--', p(1:2), [s s], 'k--', 'LineWidth', 2);    
hold off;
set(gca, 'FontSize', 12);
set(gcf, 'Units', 'Centimeters');
set(gcf, 'Position', [0 0 10 8]);
set(gcf, 'PaperPositionMode', 'auto');
if save_fig
    save_fig_to_files('Saving the dpH2O graph to files...', 'fig7b_dph2o');
end
% Show activity coefficients (NOT FOR ARTICLE, JUST FOR DEBUG)
xv = [0, 0.001:0.001:0.25];
xi = xv./(1 + xv);
mv = xi*1000./(Mw*(1 - xv));
R = getR();
T = 298.15;
[~,~,~,mu1_ex, mu2_ex] = td_model(xv, T, a, a_to_g);
lng2 = (mu2_ex ./ (R .* T)) / 2;
lng2 = lng2 - lng2(1) + log(1 - 2*xi);
figure;
subplot(1,2,1);
plot(mv,lng2,'k-','LineWidth',2);
xlabel('{\it{m}}_{NH_2SO_3NH_4}', 'FontSize', 14);
ylabel('ln \gamma_\pm', 'FontSize', 14);
subplot(1,2,2);
plot(mv,exp(lng2),'k-','LineWidth',2);
xlabel('{\it{m}}_{NH_2SO_3NH_4}', 'FontSize', 14);
ylabel('\gamma_\pm', 'FontSize', 14);
end

function p = p_H2O(x,T,a,a_to_g)
%p0 = 10.^(7.31549 - 1794.88./(T - 34.764));
R = getR();
[~, p0] = lnp0_H2O_kPa(T);
lna = td_model(x,T,a,a_to_g) ./ (R * T);
p = p0.*exp(lna);
end
