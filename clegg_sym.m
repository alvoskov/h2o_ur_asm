% Produces symbolic expressions for chemical potentials of components
% for Pitzer-Simonson-Clegg model by means of MATLAB Symbolic Math Toolbox.
%
% Two cases of binary systems are considered:
% 1) MX-SOLVENT (where MX is 1-1 electrolyte)
% 2) S1-S2 (where S1 and S2 are molecular species)

% Programming: Alexey Voskov
% License: MIT
function clegg_sym
conf_ions_sym
neutral_sym
end

function conf_ions_sym
syms xi x x_ W U p Ix0 Ax
xi = x / (1 + x);

display('========== MX-SOLVENT MODEL ==========');
% Ideal part
display('---------- Ideal part ---------');
gid = (1 + x) * (2*xi*log(xi) + (1 - 2*xi)*log(1 - 2*xi)) + 2*x*log(2);
diff_Gfunc(gid);

% Excess Gibbs energy
display('---------- Excess part ---------');
Gex = (1 + x) * ( 2*xi*(1 - 2*xi)*(W + U*2*xi))
diff_Gfunc(Gex)

% Pitzer-Simonson part
display('---------- Pitzer-Simonson part ---------');
Ix = xi;
GDH = -(1 + x) * (4*Ax*Ix / p) * log ( (1 + p*Ix^0.5) / (1 + p*Ix0^0.5) )
diff_Gfunc(GDH)
end

function diff_Gfunc(Gex)
syms x x_
dGex_dx = simplify(subs(diff(Gex, x), x, x_ / (1 - x_)));
mu1ex = simplify(subs(Gex - x*dGex_dx, x, x_ / (1 - x_)))
mu2ex = simplify(subs(Gex + (1 - x)*dGex_dx, x, x_ / (1 - x_)))
ctrl = eval(simplify(subs((1-x)*mu1ex + x*mu2ex - Gex, x, x_ / (1 - x_))))
if ~isnumeric(ctrl) || ctrl ~= 0
    error('Bad derivatives');
end
end

function neutral_sym
syms x W U V
display('========== S1-S2 MODEL ==========');
g = x*(1-x)*(W + U*(1 - 2*x) + V*(x^2 + (1-x)^2))
mu1 = simplify(g - x*diff(g, x))
mu2 = simplify(g + (1 - x)*diff(g, x))
end
