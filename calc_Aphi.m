% CALC_APHI Approximation of Aphi coefficient for Debye-Huckel 
% contribution at 0.1MPa and T=263.15-433.15 K. It is used in
% Pitzer-Simonson-Clegg model. See water_prop/approx_Aphi.m
% for details.

% Programming: Alexey Voskov
% License: MIT
function Aphi = calc_Aphi(T)
Aphi = -0.23746 - 5.5391e-3*T + 5.3511e-6*T.^2 + 0.104524*T.^0.5;
end