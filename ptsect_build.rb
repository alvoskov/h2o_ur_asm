#TERNAPI
#
# TernAPI-C script for construction of the p,T-sections
# of the H2O(1)-UREA(2)-NH4SO3NH2(3) system
#
# (C) 2016 Alexey Voskov
# License: MIT
#

# Load YAML file with our thermodynamic model
diag = nil
File.open('model_pdh.yaml') do |fp| 
	yaml = YAML.load(fp)
	diag = PhaseDiagram.new(yaml)
end

# Set Gibbs-Roozeboom triangle layout
gtcfg = diag.gibbs_triangle_cfg
gtcfg['boxsize'] = 350
gtcfg['phbound_width'] = 1
gtcfg['tieline_width'] = 1
gtcfg['gridlines'] = false
gtcfg['ticklabels'] = false
gtcfg['xstep'] = 0.2
gtcfg['sign_regions'] = false
diag.gibbs_triangle_cfg = gtcfg

# Build isothermal sections
[[249.55, 'b'], [264.95, 'c'], [298.15, 'd']].each do |params|
	filename = "fig12#{params[1]}"
	diag.set_parameter('t', params[0])
	diag.calculate
	File.open("#{filename}_raw.eps", "w") {|fp| fp << diag.to_eps }
	# Create EPS (with fonts converted to curves), PDF and PNG (300 DPI) versions
	`eps2eps -dNOCACHE #{filename}_raw.eps #{filename}.eps`
	`epstopdf fig12#{params[1]}.eps`
	`gs -r300 -sDEVICE=png16m -dBATCH -dNOPAUSE -o #{filename}.png #{filename}.pdf`
end
