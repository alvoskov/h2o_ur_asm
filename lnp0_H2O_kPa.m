% LNP0_H2O_KPA  Calculates water saturated vapour pressure at the pressure
% of saturation. The dependency used in the program is based on 
% IAPWS_IF97 and is valid for T = 280 - 370 K
%
% Usage:
%   [lnp0_kPa, p0_kPa] = lnp0_H2O_kPa(T)
% Inputs:
%   T -- temperature, K (can be matrix)

function [lnp0_kPa, p0_kPa] = lnp0_H2O_kPa(T)
if any(T(:) < 280 | T(:) > 370)
    warning('Some T values are out of 280-370 K range');
end
lnp0_kPa = 68.73133 - 7193.738./T - 7.896606.*log(T) + 5.17166e-3.*T;
if nargout > 1
    p0_kPa = exp(lnp0_kPa);
end
end