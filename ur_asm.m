% UR_ASM  UREA(1)-ASM(2) binary system thermodynamic modeling
% UREA --- urea NH2CONH2
% ASM  --- ammonium sulfamate NH2SO3NH4
%
% See also compProp

% Programming: Alexey Voskov
% License: MIT
function ur_asm(save_fig)
if nargin == 0
    save_fig = true;
end
close all;
% Experimental phase diagram of UR-ASM system
% xASM - Ammonium sulfamate molar fraction
% ts(C) - solidus temperature (celsius)
% tl(C) - liquidus temperature (celsius)
data_liq = [
%xASM   ts(C)   tl(C)
0.0026	64.4	133.5
0.0107	65.2	131.8
0.0274	65.7	129.0
0.0557	65.1	122.6
0.0850	65.7	117.3
0.1156	65.1	109.1
0.1831	65.2	95.4
0.2615	65.3	83.5
0.3003	65.5	78.1
0.4358	65.5	nan
0.4929	65.8	84.0
0.5495	65.4	88.4
0.5962	65.4	96.6
0.6752	65.3    105.2
0.6780	65.6	nan
0.7432	65.0	112.3
0.8247	65.1	118.1
0.9122	64.5	125.4];

ii_s = ~isnan(data_liq(:,2)); ii_l = ~isnan(data_liq(:,3));
xs = data_liq(ii_s, 1); Ts = data_liq(ii_s, 2) + 273.15;
xl = data_liq(ii_l, 1); Tl = data_liq(ii_l, 3) + 273.15;
% Nonlinear regression: parameters mapping
a0 = [0 0 0];
a_to_g = @(a, T) [a(1) + a(2)*T, a(3), 0];    
% Nonlinear regression: target function
function delta = minfunc(a)
    [Tl_c, x_eut_c, T_eut_c] = calc_liquidus_Tv(xl, a, stab_param, a_to_g);
    delta = [(Tl_c - Tl)./Tl; (T_eut_c - Ts)./Ts]*1000;
end

% Stability parameters
ASMprop = compProp('ASM');
URprop = compProp('UR');
dmG_ASM = ASMprop.dmG_func;
dmG_UREA = URprop.dmG_func;
stab_param = struct('dmG1', dmG_UREA, 'dmG2', dmG_ASM, 'comp2', 'NH_4SO_3NH_2');

a = td_model_fit(a0, @minfunc);
fprintf('Teut(empirical) = %.2f\n', mean(Ts));
    
% Calculate branches of the phase diagram (for export in .csv file
% for Elsevier interactive plots)
fd = fopen('output/iplot_ur_asm.csv', 'w');
fprintf(fd, 'x3(%%),T(K)\n');
[~, x_eut_calc, T_eut_calc] = calc_liquidus_Tv(0.5, a, stab_param, a_to_g);
Tv_floor = ceil(T_eut_calc);
% a) left branch
fprintf(fd, '%.3f,%.2f\n', [0; 405.85]);
Tv = 405:(-1):Tv_floor;
xv = calc_liquidus_xv(Tv,1,a,stab_param,a_to_g);
fprintf(fd, '%.3f,%.2f\n', [xv*100; Tv]);
% b) eutectic point
fprintf(fd, '%.3f,%.2f\n', x_eut_calc*100, T_eut_calc);
% c) right branch
Tv = sort([Tv_floor:1:406]);
xv = calc_liquidus_xv(Tv,2,a,stab_param,a_to_g);
fprintf(fd, '%.3f,%.2f\n', [xv*100; Tv]);
fprintf(fd, '%.3f,%.2f\n', [100; 406.1]);
fclose(fd);

% Show the diagram (calculated vs experimental)
data = struct('x',{xs,xl}, 'T', {Ts,Tl}, 'marker', {'rd', 'bo'});
show_xT_diagram(data,a,stab_param, a_to_g);
if save_fig
    save_fig_to_files('Saving the phase diagram to files...', 'fig4_ur_asm');
end
end
