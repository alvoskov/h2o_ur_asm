% H2O_UR Water(1) -- Urea(2) binary system thermodynamic model fitting
% It is based on Pitzer-Simonson-Clegg model for non-electrolytes with 
% temperature-dependent parameters
%
% References to original papers with experimental data can be taken from
% [1] Comments in this file
% [2] http://dx.doi.org/10.1021/je400255c (mainly for SLE data)
%
% Usage:
%   h2o_ur();
%   h2o_ur(saveFigs);
%
% saveFigs -- omitted: it will be requested if saving a part of figures
%             figures to files is required
%             true/false save/don't save a part of figures to file
%
% NOTE:
%   x -- x(UREA) (urea is SECOND component)

% Programming: Alexey Voskov
% License: MIT
function h2o_ur(saveFigs)
global H2Oprop URprop
optimize = true;
%-------------------------------------------------------------------------
%- Thermodynamic model integrity test
%-------------------------------------------------------------------------
nparams = 11;
g00 = repmat([5, -5, 3], 1, 20);
g00 = g00(1:nparams);
fprintf('===== Thermodynamic model integrity test =====\n');
%---------------------------------------------
fprintf('----- Testing chemical potentials\n');
dx = 1e-6;
x = 0.01:0.01:0.99;
T = 298.15;
dmixG = td_func(g00, x, T, 'dmixG');
dmu1 = td_func(g00, x, T, 'dmu1');
dmu2 = td_func(g00, x, T, 'dmu2');
% a) sum of chemical potentials
if max(abs(dmixG - (1 - x).*dmu1 - x.*dmu2)./abs(dmixG)) > 1e-12
    error('dmixG - ((1-x)*dmu1 + x*dmu2) test failed');
else
    fprintf('dmixG - ((1-x)*dmu1 + x*dmu2) test passed\n');
end
% b) numerical calculation of chemical potentials
dmixG_dx = (td_func(g00, x + dx, T, 'dmixG') - td_func(g00, x - dx, T, 'dmixG')) ./ (2*dx);
dmu1_num = dmixG - x.*dmixG_dx;
dmu2_num = dmixG + (1 - x).*dmixG_dx;
if max(abs(dmu1_num - dmu1)./abs(dmu1)) > 1e-6
    error('dmu1 numeric test failed');
else
    fprintf('dmu1 numeric test passed\n');
end
if max(abs(dmu2_num - dmu2)./abs(dmu2)) > 1e-6
    error('dmu2 numeric test failed');
else
    fprintf('dmu2 numeric test passed\n');
end
%---------------------------------------------
fprintf('----- Testing partial enthalpies\n');
dmixH = td_func(g00, x, T, 'dmixH');
dH1 = td_func(g00, x, T, 'dH1');
dH2 = td_func(g00, x, T, 'dH2');
% a) sum of partial enthalpies
if max(abs(dmixH - (1 - x).*dH1 - x.*dH2)./abs(dmixH)) > 1e-12
    error('dmixH - ((1-x)*dH1 + x*dH2) test failed');
else
    fprintf('dmixH - ((1-x)*dH1 + x*dH2) test passed\n');
end
% b) numerical calculation of partial enthalpy
dmixH_dx = (td_func(g00, x + dx, T, 'dmixH') - td_func(g00, x - dx, T, 'dmixH')) ./ (2*dx);
dH1_num = dmixH - x.*dmixH_dx;
dH2_num = dmixH + (1 - x).*dmixH_dx;
if max(abs(dH1_num - dH1)./abs(dH1)) > 1e-5
    error('dH1 numeric test failed');
else
    fprintf('dH1 numeric test passed\n');
end
if max(abs(dH2_num - dH2)./abs(dH2)) > 1e-5
    error('dH2 numeric test failed');
else
    fprintf('dH2 numeric test passed\n');
end
%---------------------------------------------
fprintf('----- Testing integral enthalpies\n');
dT = 1e-5;
x = 0.35;
T = 200:10:500;
% a) integral enthalpies
dmixH = td_func(g00, x, T, 'dmixH');
dmixH_num = -T.^2 .* (td_func(g00, x, T+dT, 'dmixG')./(T+dT) - td_func(g00, x, T-dT, 'dmixG')./(T-dT)) ./ (2*dT);
if max(abs(dmixH - dmixH_num)./abs(dmixH)) > 1e-5
    error('dmixH numeric test failed');
else
    fprintf('dmixH numeric test passed\n');
end
%---------------------------------------------
fprintf('----- Testing heat capacities\n');
dT = 1e-5;
x = 0.33;
T = 200:10:500;
Cp = td_func(g00, x, T, 'dmixCp');
Cp_num = (td_func(g00, x, T + dT, 'dmixH') - td_func(g00, x, T - dT, 'dmixH'))./(2*dT);
if max(abs(Cp - Cp_num)./abs(Cp)) > 1e-5
    error('dmixCp numeric test failed');
else
    fprintf('dmixCp numeric test passed\n');
end
fprintf('\n');

%-------------------------------------------------------------------------
%- Stability parameters and another variables
%-------------------------------------------------------------------------
close all;
% Urea and water thermodynamic properties
H2Oprop = compProp('W');
URprop = compProp('UR');

%-------------------------------------------------------------------------
%- Load experimental data (SLE + thermodynamics)
%-------------------------------------------------------------------------
sle_series = load_sle_data;
phi_series = load_phi_data;
%vle_series = load_p_data;
dHdil_series = load_dHdil_data;
dHsol_series = load_dHsol_data;
dHsol_inf_series = load_dHsol_inf_data;
dH1_series = load_dHw_data;
dH2_series = load_dHur_dat;
Cp_series = load_Cp_data;

data_series = {sle_series, phi_series, dHdil_series, dHsol_series, dHsol_inf_series, dH1_series, dH2_series, Cp_series};

%-------------------------------------------------------------------------
%- Estimate model parameters
%-------------------------------------------------------------------------
% ----- Some useful function
leq_func = @(g00, x, T) ( H2Oprop.dmG_func(T)  + td_func(g00, x, T, 'dmu1'));
req_func = @(g00, x, T) ( URprop.dmG_func(T) + td_func(g00, x, T, 'dmu2'));
eut_func = @(g00, xv) ([leq_func(g00, xv(1), xv(2)); req_func(g00, xv(1), xv(2))]);

% Try to estimate g00 parameter
opt = optimset('Display', 'iter', 'MaxFunEvals', 10000, 'MaxIter', 1000, 'TolFun', 1e-6);
g00 = zeros(1, nparams);

if optimize
    [g00,~,res,exitflag,~,~,J] = lsqnonlin(@delta_g00_UR_W, g00, [], [], opt);
    fprintf('exitflag: %d\n', exitflag);
    fprintf('%.10e ', g00); fprintf('\n');
    res = res(:);
        
    if exitflag > 0
        extra_digits = 2;
        while true        
            [~, ~, g00_rounded] = display_parameters(g00, res, J, extra_digits);
            res = delta_g00_UR_W(g00_rounded)';
            sum_rounded = sum(res.^2);
            sum_accurate = sum(delta_g00_UR_W(g00).^2);
            if abs(sum_rounded - sum_accurate)/sum_accurate < 0.001
                display_parameters(g00_rounded, res, J, extra_digits);
                break;
            end
            extra_digits = extra_digits + 1;
        end            
    else
        warning('Bad exitflag!');
    end
end

delta_g00_UR_W(g00, true);
[~, str] = params_mapper(g00);
disp(str);


% ----- Convexity control
Tv = 200:10:500;
isconv_old = NaN;
for i = 1:numel(Tv)
    [isconv, np, nk, xmin, xmax] = is_Gfunc_convex(g00, Tv(i));
    if ~isnan(isconv_old) && isconv_old ~= isconv
        Tv_hr = linspace(Tv(i-1), Tv(i), 11);
        for j = 2:(numel(Tv_hr)-1);
            [isconv_hr, np_hr, nk_hr, xmin_hr, xmax_hr] = is_Gfunc_convex(g00, Tv_hr(j));
            fprintf('T = %.2f %d/%d; %.5f %.5f\n', Tv_hr(j), np_hr, nk_hr, xmin_hr, xmax_hr);
        end
        
    end
    isconv_old = isconv;
    fprintf('T = %.2f %d/%d; %.5f %.5f\n', Tv(i), np, nk, xmin, xmax);
end


% ----- Calculate the diagram using estimated parameters (for the diagram) -----
% 1) find the eutectic
xT_eut = fsolve(@(xT) eut_func(g00, xT), [0.5 300]);
% 2) right branch (liquid - urea)
xv = linspace(xT_eut(1) - 0.01, 1, 150);
Tv = liquidus_solver(xv, 300*ones(size(xv)), @(x,T) req_func(g00, x, T));
% 3) left branch (ice - liquid)
xv_l = linspace(0, xT_eut(1) + 0.01, 150);
Tv_l = liquidus_solver(xv_l, 300*ones(size(xv_l)), @(x,T) leq_func(g00, x, T));

%-------------------------------------------------------------------------
%- Text output and visualization
%-------------------------------------------------------------------------
% ----- Calculate branches of the phase diagram (for export in .txt file)
% a) right branch
fd = fopen('output/ur_h2o_sol.txt', 'w');
Tv = [260:1:405; (260:1:405)+0.15]; Tv = Tv(:);
xv = liquidus_solver_xvar(Tv, @(x,T) req_func(g00, x, T));
fprintf(fd, '----- SOLUBILITY OF UREA IN WATER -----\n');
fprintf(fd, '  %10s %10s  %10s %10s\n', 'T,K', 'x(UR)', 'T,K', 'x(UR)');
for i = 1:2:numel(Tv)
    fprintf(fd, '  %10.2f %10.5f  %10.2f %10.5f\n', Tv(i), xv(i), Tv(i+1), xv(i+1));
end
fprintf('\n');
% b) left branch
Tv = [260:1:273; (260:1:273)+0.15]; Tv = Tv(:);
xv = liquidus_solver_xvar(Tv, @(x,T) leq_func(g00, x, T));
fprintf(fd, '----- ICE-WATER EQUILIBRIUM IN UREA-WATER SYSTEM -----\n');
fprintf(fd, '  %10s %10s  %10s %10s\n', 'T,K', 'x(UR)', 'T,K', 'x(UR)');
for i = 1:2:numel(Tv)
    fprintf(fd, '  %10.2f %10.5f  %10.2f %10.5f\n', Tv(i), xv(i), Tv(i+1), xv(i+1));
end
fclose(fd);
% ----- Calculate branches of the phase diagram (for export in .csv file
% for Elsevier interactive plots)
fd = fopen('output/iplot_w_ur.csv', 'w');
fprintf(fd, 'x2(%%),T(K)\n');
Tv_floor = ceil(xT_eut(2));
% a) left branch
fprintf(fd, '%.3f,%.2f\n', [0; 273.15]);
Tv_liq = [273.15];
xv_liq = [0];
Tv = 273:(-1):Tv_floor; Tv_liq = [Tv_liq, Tv];
xv = liquidus_solver_xvar(Tv, @(x,T) leq_func(g00, x, T)); xv_liq = [xv_liq, xv];
fprintf(fd, '%.3f,%.2f\n', [xv*100; Tv]);
% b) eutectic point
fprintf(fd, '%.3f,%.2f\n', xT_eut(1)*100, xT_eut(2));
% c) right branch
Tv = sort([Tv_floor:1:405, (-10:5:130)+273.15]); Tv_liq = [Tv_liq, Tv];
xv = liquidus_solver_xvar(Tv, @(x,T) req_func(g00, x, T)); xv_liq = [xv_liq, xv];
fprintf(fd, '%.3f,%.2f\n', [xv*100; Tv]);
fprintf(fd, '%.3f,%.2f\n', [100; 405.85]);
fclose(fd);

% ----- Print the parameters of model -----
fprintf('SPECIAL POINTS\n');
prn_special_point('Eutectic', xT_eut);

% ----- Show the diagram -----
close all;
markers = {'ko', 'kd', 'ks', ...
    'kv', 'k^', 'k<', 'k>', 'k*', 'bh', 'r*'};
ser = sle_series.series;
ser.Bab10.x = [ser.Bab10.x, ser.Bab10_eut.x];
ser.Bab10.T = [ser.Bab10.T, ser.Bab10_eut.T];
ser.Cha38.x = [ser.Cha38.x, ser.Cha38_eut.x];
ser.Cha38.T = [ser.Cha38.T, ser.Cha38_eut.T];
ser = rmfield(ser, 'Bab10_eut');
ser = rmfield(ser, 'Cha38_eut');

hold on;
ff = fields(ser);
for ind = 1:numel(ff);
    f = ff{ind};
    plot(ser.(f).x, ser.(f).T, markers{ind});
end
plot(xv_liq, Tv_liq, 'k-');
plot([0 1], [xT_eut(2) xT_eut(2)], 'k-');
hold off;
%{
plot(ser.Spe02.x, ser.Spe02.T, 'ko', ...
     ser.Pin25.x, ser.Pin25.T, 'kd', ...
     ser.Jan30.x, ser.Jan30.T, 'ks', ...
     ser.Mil34.x, ser.Mil34.T, 'kv', ...
     [ser.Cha38.x, ser.Cha38_eut.x], [ser.Cha38.T, ser.Cha38_eut.T], 'k^', ...
     ser.Shn32.x, ser.Shn32.T, 'k<', ...
     ser.Kak41.x, ser.Kak41.T, 'k>', ...     
     [ser.Bab10.x ser.Bab10_eut.x], [ser.Bab10.T, ser.Bab10_eut.T], 'k*', ...
     ser.Lee72.x, ser.Lee72.T, 'bh', ...     
     ser.Points.x, ser.Points.T, 'r*', ...
     xv, Tv, 'k-', xv_l, Tv_l, 'k-', ...
%}
axis([0 1 250 420]);
xlabel('{\it x}_{(NH_2)_2CO}', 'FontSize', 12);
ylabel('{\it T}, K', 'FontSize', 12);
legend(ff{:}, 'Location', 'Best');

% ----- Show figures with experimental thermodynamic data -----
for i = 1:numel(data_series)
    ser = data_series{i};
    disp(ser, g00);
    ser.plotRelativeDeviations(g00);
end

% ----- Save some figures into files (if requested by user) -----
if nargin == 0
    yn = input('Do you want to save figures to files (Y/N)?', 's');
else
    if saveFigs
        yn = 'y';
    else
        yn = 'n';
    end
end
if strcmpi(yn, 'y')
    close all;
    disp('Saving figures to files...');
    prnfile = {'fig8_phi', 'fig9_dHdil', 'figX_dHsol', 'figX_dH2', 'figX_Cp'};
    prnser  = {phi_series, dHdil_series, dHsol_series, dH2_series, Cp_series};
    for i = 1:numel(prnfile)
        ser = prnser{i};
        ser.noLegend = true;
        ser.plotRelativeDeviations(g00, prnfile{i});
    end
end


% ----- Show coefficients -----
% a) visualization
Tv = 233:10:413;
[Wg, Ug, Vg] = params_to_WUVg(g00, Tv);
[Wh, Uh, Vh] = params_to_WUVh(g00, Tv);
figure;
plot(Tv, Wg, 'r-', Tv, Ug, 'b-', Tv, Vg, 'k-', 'LineWidth', 2);
legend('W_g', 'U_g', 'V_g');

figure;
plot(Tv, Wh, 'r-', Tv, Uh, 'b-', Tv, Vh, 'k-', 'LineWidth', 2);
legend('W_h', 'U_h', 'V_h');

% b) tables
Tv = [250 298.15 300 350 400 450];
[Wg, Ug, Vg] = params_to_WUVg(g00, Tv);    
fprintf('%10s %10s %10s %10s\n', 'T', 'Wg', 'Ug', 'Vg');
for i = 1:numel(Tv)
    fprintf('%10.4f ', [Tv(i), Wg(i), Ug(i), Vg(i)]);
    fprintf('\n');
end

[Wh, Uh, Vh] = params_to_WUVh(g00, Tv);    
fprintf('%10s %10s %10s %10s\n', 'T', 'Wh', 'Uh', 'Vh');
for i = 1:numel(Tv)
    fprintf('%10.4f ', [Tv(i), Wh(i), Uh(i), Vh(i)]);
    fprintf('\n');
end

[Wc, Uc, Vc] = params_to_WUV_Cp(g00, Tv);    
fprintf('%10s %10s %10s %10s\n', 'T', 'W_Cp*100', 'U_Cp*100', 'V_Cp*100');
for i = 1:numel(Tv)
    fprintf('%10.3f ', [Tv(i), Wc(i)*100, Uc(i)*100, Vc(i)*100]);
    fprintf('\n');
end
 
%-------------------------------------------------------------------------
%- Nested functions
%-------------------------------------------------------------------------
    function delta = delta_g00_UR_W(g00, verbose)
        if nargin < 2
            verbose = false;
        end
        lambda = 0; % Regularization parameter
        % Tikhonov regularization (to prevent miscibility gaps)
        deltaReg = lambda * g00(:)';
        % Data series
        delta_series = [];
        for jj = 1:numel(data_series)
            delta_series = [delta_series, data_series{jj}.residuals(g00)];
        end
        % Generation of output
        delta = [delta_series deltaReg];
        if verbose
            sigmafunc = @(res) sqrt(sum(res.^2) ./ (numel(res) - 1));
            fprintf('%.10e ', g00); fprintf('\n');            
            fprintf('Information about the sums\n');
            sum_names = {'REG', 'TOTAL'};
            sum_delta = {deltaReg, delta};
            sum_T = {[200 500], [200 500]};
            
            for jj = 1:numel(data_series)
                sum_names{end+1} = data_series{jj}.descr;
                sum_delta{end+1} = data_series{jj}.residuals(g00);
                sum_T{end+1} = data_series{jj}.yfunc_inp.T;
            end
            
            for sind = 1:numel(sum_names)
                sdelta = sum_delta{sind};
                fprintf('  %7s:   SUM = %6.2e, STD = %5.2f%%, MAXD = %5.2f%% POINTS = %d T=%.2f-%.2f\n', ...
                    sum_names{sind}, sum(sdelta.^2), sigmafunc(sdelta)*100, max(abs(sdelta))*100, numel(sdelta), ...
                    min(sum_T{sind}), max(sum_T{sind}) );
            end
            fprintf('\n');
        end
    end
end


function prn_special_point(name, xT)
% Function prints the coordinates of the special point
    fprintf('%s: x = %f, T = %f K (%f C)\n', name, xT(1), xT(2), xT(2) - 273.15);
end

function [x, T] = dat_to_xT(dat)
% Converts dat matrix to the x and T vectors
% dat - 2xN or Nx2 matrix. Each row (or column) is the (x,t) pair
%       where x is the molar fraction, t is the temperature in C
%
if size(dat, 2) == 2
    x = dat(:,1)'; T = dat(:,2)' + 273.15;
elseif size(dat, 1) == 2
    x = dat(1,:);  T = dat(2,:)  + 273.15 ;
else
    error('Invalid dat value');
end
end

function Tcalc = liquidus_solver(xdat, Tdat, eq_func)
try
    Tcalc = bisect_vectorized(eq_func, xdat, 200*ones(size(xdat)), 500*ones(size(xdat)));
catch
    warning('liquidus_solver: using fsolve');
    opt = optimset('Display', 'off');
    Tcalc = nan(size(Tdat));
    for i=1:numel(Tdat)
        Tcalc(i) = fsolve(@(T) eq_func(xdat(i), T), Tdat(i), opt);
    end
end
end

function xcalc = liquidus_solver_xvar(Tdat, eq_func)
opt = optimset('Display', 'off');
xcalc = nan(size(Tdat));
for i=1:numel(Tdat)
    xcalc(i) = fsolve(@(x) eq_func(x, Tdat(i)), 0.5, opt);
end
end

% eqfunc(param, y)
function xC = bisect_vectorized(eq_func, param, xA, xB)
fA = eq_func(param, xA);
fB = eq_func(param, xB);
while true
    xC = xA + 0.5*(xB - xA);
    fC = eq_func(param, xC);
    
    sA = sign(fA).*sign(fC);
    sB = sign(fC).*sign(fB);
    
    iiA = sA <= 0;
    iiB = sB <= 0;
    
    if any((sA > 0) & (sB > 0))      
        error('Cannot solve the equation');
    end
    
    xB(iiA) = xC(iiA); fB(iiA) = fC(iiA);
    xA(iiB) = xC(iiB); fA(iiB) = fC(iiB);
    
    % Loop condition
    if max(xB - xA) < eps * max(abs(xC))
        break;
    end
end
end

function lnp_bar = lnp0_H2O(T)
lnp_kPa = lnp0_H2O_kPa(T);
lnp_bar = lnp_kPa - log(100);
end

function Cp = Cp_H2O_L(T)
global H2Oprop
Cp = 3.737397e+02 -4.634464e-01*T + 2.986921e-04*T.^2 + 9.504162e+04./T -3.11213e+04.*T.^(-0.7);
Cp = Cp * H2Oprop.M;
end

function Cp = Cp_UR_L(T)
Cp = 253.65 + 33.95 - 2763.3*T.^(-0.5);
end

function [a, str] = params_mapper(a0)
a = zeros(1,18);
inds = [1:3, 7:13, 15];
%inds = [1:3, 7:15];
if numel(inds) ~= numel(a0)
    warning('a0 and inds sizes are not matching. Truncating.');
end
a(inds) = a0(1:numel(inds));
if nargout == 2
    str = '';
    str = [str, fprintf('Wg = (%.10f)./t + (%.10f) + (%.10f).*log(T) + (%.10f).*t + (%.10f).*t.^2 + (%.10f).*log(T)./t\n', a([1 4 7 10 13 16]))];
    str = [str, fprintf('Ug = (%.10f)./t + (%.10f) + (%.10f).*log(T) + (%.10f).*t + (%.10f).*t.^2 + (%.10f).*log(T)./t\n', a([2 5 8 11 14 17]))];
    str = [str, fprintf('Vg = (%.10f)./t + (%.10f) + (%.10f).*log(T) + (%.10f).*t + (%.10f).*t.^2 + (%.10f).*log(T)./t\n', a([3 6 9 12 15 18]))];    
end
end

function [Wg, Ug, Vg] = params_to_WUVg(a, T)
a = params_mapper(a);
t = T ./ 300;
Wg = a(1)./t + a(4) + a(7).*log(T) + a(10).*t + a(13).*t.^2 + a(16).*log(T)./t;
Ug = a(2)./t + a(5) + a(8).*log(T) + a(11).*t + a(14).*t.^2 + a(17).*log(T)./t;
Vg = a(3)./t + a(6) + a(9).*log(T) + a(12).*t + a(15).*t.^2 + a(18).*log(T)./t;
end

function [Wh, Uh, Vh] = params_to_WUVh(a, T)
a = params_mapper(a);
t = T ./ 300;
Wh = a(1)./t - a(7) - a(10).*t - 2*a(13).*t.^2 + a(16).*(log(T) - 1)./t;
Uh = a(2)./t - a(8) - a(11).*t - 2*a(14).*t.^2 + a(17).*(log(T) - 1)./t;
Vh = a(3)./t - a(9) - a(12).*t - 2*a(15).*t.^2 + a(18).*(log(T) - 1)./t;
end

function [Wc, Uc, Vc] = params_to_WUV_Cp(a, T)
a = params_mapper(a);
T0 = 300;
Wc = - a(7)./T - 2*a(10)/T0 - 6*a(13).*T/T0.^2 + a(16)./T.^2 .* T0;
Uc = - a(8)./T - 2*a(11)/T0 - 6*a(14).*T/T0.^2 + a(17)./T.^2 .* T0;
Vc = - a(9)./T - 2*a(12)/T0 - 6*a(15).*T/T0.^2 + a(18)./T.^2 .* T0;
end

function [isconvex, npoints, nchpoints, xmin, xmax] = is_Gfunc_convex(g00, T)
xv = 0:0.0001:1; xv(xv == 0) = 1e-12; xv(xv == 1) = 1 - 1e-12;
Gv = td_func(g00, xv, T, 'dmixG');
xv_ext = [0 1 xv];
k = unique(convhull(xv_ext, [1e5 1e5 Gv]));
npoints = numel(xv) + 2;
nchpoints = numel(k);
if nchpoints == npoints
    isconvex = true;
    xmin = NaN;
    xmax = NaN;
else
    xv_nonconv = setxor(xv_ext(k), xv_ext);
    xmin = min(xv_nonconv);
    xmax = max(xv_nonconv);
    isconvex = false;
end
end

function F = td_func(p, x, T, funcname)
R = getR();
% Extract parameters
[Wg, Ug, Vg] = params_to_WUVg(p, T);
[Wh, Uh, Vh] = params_to_WUVh(p, T);
[Wc, Uc, Vc] = params_to_WUV_Cp(p, T);
switch funcname
    case 'dmu1'
        F = x.^2.*(Wg + Ug.*(3 - 4*x) + Vg.*(3 - 8*x + 6*x.^2)) + log(1 - x);
    case 'dmu2'
        F = (1 - x).^2.*(Wg + Ug.*(1 - 4*x) + Vg.*(1 - 4*x + 6*x.^2)) + log(x); 
    case 'dH1'
        F = x.^2.*(Wh + Uh.*(3 - 4*x) + Vh.*(3 - 8*x + 6*x.^2));
    case 'dH2'
        F = (1 - x).^2.*(Wh + Uh.*(1 - 4*x) + Vh.*(1 - 4*x + 6*x.^2));
    case 'dmixG'
        F = x.*(1-x).*(Wg + Ug.*(1 - 2*x) + Vg.*((1-x).^2 + x.^2)) + ((1-x).*log(1-x) + x.*log(x));
    case 'dmixH'
        F = x.*(1-x).*(Wh + Uh.*(1 - 2*x) + Vh.*((1-x).^2 + x.^2));
    case 'dmixCp'
        F = x.*(1-x).*(Wc + Uc.*(1 - 2*x) + Vc.*((1-x).^2 + x.^2));        
    otherwise
        error('Unknown function %s', funcname);
end
F = F .* R .* T;
end

function x = UW_wtox(w)
    global H2Oprop URprop
    Mw = H2Oprop.M; % M(H2O), g/mol
    Mur = URprop.M; % M(UREA), g/mol
    x = (w ./ (w + (100-w)*Mur/Mw)); % w - w(UR)
end

function x = UW_mtox(m)
    global H2Oprop
    Mw = H2Oprop.M; % M(H2O), g/mol
    x = (m ./ (m + 1000./Mw)); % m - m(UR)
end

function m = UW_xtom(x)
    global H2Oprop
    Mw = H2Oprop.M; % M(H2O), g/mol
    m = (x ./ ((1 - x) .* Mw./1000)); % x - x(UR)
end

function [phi, lna, p] = NaCl_phi_298(m)
% NaCl osmotic coefficient (+ water activity and vapour pressure)
% at 298K and 0.1MPa
%
% Formula is taken from 
% [1] Archer D.G. Thermodynamic properties of the NaCl-H2O system.
% II. Thermodynamic properties of NaCl(aq), NaCl*2H2O(cr), and phase
% equilibria // J. Phys. Chem. Ref. Data. 1992. 21(4). P. 793-829.
global H2Oprop
T0 = 298;
Aphi = 0.3914;
bMX0 = 0.08055;
bMX1 = 0.2630;
CMX0 = 0.2682e-3;
CMX1 = -0.01022;
a = 2.0;
b = 1.2;
a2 = 2.5;
Mw = H2Oprop.M * 1e-3;

I = m;
phi = 1 - Aphi.*I.^0.5./(1 + b.*I.^0.5) + ...
    m.*(bMX0 + bMX1.*exp(-a.*I.^0.5)) + ...
    2*m.^2.*(CMX0 + CMX1.*exp(-a2.*I.^0.5));

lna = -2*m.*Mw .* phi;
p = exp(lna).*exp(lnp0_H2O(T0));
end

%=========================================================================
%=== THERMODYNAMIC EXPERIMENTAL DATA
%=========================================================================

function ser = load_sle_data
global H2Oprop URprop
Speyers02_dat = [ %1902
    0.0     11.0    19.8    31.7    51.4    69.5  % tC
    67.4    87.5    97.5    131.0   193.0   253.0 % gUR/100gH2O
    ];
Pinck25_dat = [ % 1925
    0.0     10.0    20.0    30.0    39.7    50.0    50.6    60.0    68.5    70.0
    67.0    84.0    104.7   136.0   165.4   205.0   206.4   246.0   295.0   314.6
    ];
Janecke30_UW_dat = [
    32.5    40.0    44.0    50.0    50.9    60.0    71.4    76.3    87.0    90.0    95.0 % w(UR),%
    -11.5   0       7       17      20      35      60      70      95      107     120  % t,C
    ];
Miller34_dat = [
    1.0000 0.9591 0.9450 0.9004 0.8891 0.8578 0.8456 0.8190 0.7702 0.7217 0.6343 0.5680 0.5095 0.4741 % x(UR)
    132.6  128.8  127.5  123.2  121.9  118.7  118.3  115.3  109.9  104.4  93.8   84.4   75.3   68.5]; % t,C
Shnidman32_dat = [
    0.2513  21.59
    0.2602  23.85
    0.2888  30.38
    0.3102  35.15
    0.3377  41.11
    0.3510  43.85
    0.4065  54.97
    0.4130  55.88
    0.4294  59.13
    0.4561  63.79
    0.4956  70.49
    ];
Kakinuma41_dat = [
    0.4338  60
    0.4610  65
    0.4903  70
    0.5204  75
    0.5517  80
    0.5843  85
    0.6190  90
    0.6542  95
    0.6910  100
    ];
Babkina10_dat = [ % Ice-LIQUID (water-urea solution)
    0.009   -3.2
    0.023   -5.1
    0.048   -5.5
    ];
Chadwell38_dat = [ % Ice-LIQUID (water-urea solution)
    %m      -dT     | m - m(urea), mol/kg
    0.3241  0.5953
    0.4315  0.7893
    0.6458  1.1698
    1.5213  2.6732
    3.3601  5.4897
    3.3696  5.5944
    4.5453  7.1506
    5.2848  8.0825
    6.0126  8.9659
    ];

Chadwell38_eut_dat = [
    8.0828  11.4142 % EUTECTIC
    8.0833  11.4146 % EUTECTIC    
    ];

Babkina10_eut_dat = [ % Eutectic data (DSC)
    0.009   -12.0
    0.023   -12.1
    0.048   -11.8
    0.070   -12.0
    0.121   -11.8
    0.186   -12.2
    0.196   -12.0
    0.209   -12.0
    ];

% [Lee72] Lee F.-M., Lahti L.E. Solubility of urea in water-alcohol
% mixtures // J. Chem. Eng. Data, 1972, 17(3), 304-306
% DOI: 10.1021/je60054a020
Lee72_dat = [ %tC, g(UR) per        100g(W)
    1.8     70.12
    10.3    85.86
    15.5    97.00
    21.0    109.6
    25.5    121.0
    30.5    135.4
    39.7    163.9
    50.1    205.2
    60.0    258.9
    ];



% [Dav31] Davis R.O.E., Black C.A. Liquid area of urea-ammonium-carbon
% dioxide system // Ind. Eng. Chem. Res. 1931, 23(11), 1280-1282.
%
% [Zha99] Zhang F.-X., Zhao P., Yang Q., Guo L.-J., Shi Q.-Z. A study
% of the isotherml solubility of ternary systems CuSO4(ZnSO4)-CO(NH2)2-H2O
% at 30C // Chemical Journal of Chinese Universities, 1999, 20(10),
% 1499-1503.
%
% [Guo09] Guo K.-N., Zhang C., Cao J.-L. Phase equilibrium of the
% quaternary NH4Cl-CO(NH2)2-H2O-H2O2 system at 15C // Journal of Hebei
% Normal University (Natural Science Edition), 2009, 33(5), 629-633.
%
% [Loe11] Loeser E., DelaCruz M., Madappalli V. Solubility of urea in
% acetonitrile-water mixtures and liquid-liquid phase separation of
% urea-saturated acetonitrile-water mixtures // J. Chem. Eng. Data, 2011,
% 56(6), 2909-2913
%
% [Lan12] Lan T.-Y., Cao J.-L., Jin H.-Y., Liu X.-W. Phase diagrams of
% Na2SO4-MgSO4-CO(NH2)2-H2O systems at 25C and their application // J.
% Chem. Eng. Data. 2012. 57. 389-393.
%
% [Nos12] Noskov M.N., Mazunin S.A. Phase balance in threefold systems
% CO(NH2)2-(NH4)2HPO4-H2O, CO(NH2)2-(NH4)2SO4-H2O, CO(NH2)2-NH4Cl-H2O at
% 25C // Sovremennyye Problemi Nauki I Obrazovaniya. 2012. 3.
% Носков М.Н., Мазунин С.А. Фазовые равновесия в тройных системах
% CO(NH2)2-(NH4)2HPO4-H2O, CO(NH2)2-(NH4)2SO4-H2O, CO(NH2)2-NH4Cl-H2O при
% 25C // Современные проблемы науки и образования. 2012. 3.
%
% [Gon16] Gong X.-M., Qiao H., Zhao B., Zhang J.-Y., Cao J.-L. Phase
% diagrams of Na2SO4-MgSO4-CO(NH2)2-H2O system at 60C // Fluid Phase
% Equilibria. 2016. 410. 23-30.
%

Points_dat = [ % tC, wUR
    15  48.00  % Guo2009
    25  53.90  % Lan2012
    25  54.40  % Noskov2012
    25  0.270*URprop.M/(0.270*URprop.M + 0.730*H2Oprop.M) * 100 % Loe11
    30  55.84  % Zhang1999
    60  70.69  % Gong2016
    65  76.92  % Davis1931
    ];

% ----- Experimental data conversion -----
% a) Urea-Liquid equilibrium monovariant line
Spe02_x = UW_wtox(100*Speyers02_dat(2,:)./(Speyers02_dat(2,:) + 100));
Spe02_T = Speyers02_dat(1,:) + 273.15;
Pin25_x = UW_wtox(100*Pinck25_dat(2,:)./(Pinck25_dat(2,:) + 100));
Pin25_T = Pinck25_dat(1,:) + 273.15;
Lee72_x = UW_wtox(100*Lee72_dat(:,2)./(Lee72_dat(:,2) + 100));
Lee72_T = Lee72_dat(:,1) + 273.15;
Cha38_x = UW_mtox(Chadwell38_dat(:,1)');
Cha38_T = 273.15 - Chadwell38_dat(:,2)';
[Jan30_x, Jan30_T] = dat_to_xT(Janecke30_UW_dat);
Jan30_x = UW_wtox(Jan30_x);
[Mil34_x, Mil34_T] = dat_to_xT(Miller34_dat);
[Shn32_x, Shn32_T] = dat_to_xT(Shnidman32_dat);
[Kak41_x, Kak41_T] = dat_to_xT(Kakinuma41_dat);
Points_x = UW_wtox(Points_dat(:,2));
Points_T = Points_dat(:,1) + 273.15;
% b) Ice-Liquid equilibrium monovariant line
[Bab10_x, Bab10_T] = dat_to_xT(Babkina10_dat);
% c) Eutectic points
[Bab10_eut_x, Bab10_eut_T] = dat_to_xT(Babkina10_eut_dat);
Cha38_eut_x = UW_mtox(Chadwell38_eut_dat(:,1)');
Cha38_eut_T = 273.15 - Chadwell38_eut_dat(:,2)';

% Create DataGroup class example
ser = DataGroup('SLE', {'T', 'x', 'type'}, @sle_func, 'rel');
add_ser_func = @(ser, name, x, T, type) ser.addSeries(name, struct('x', x, 'T', T, 'type', type*ones(size(x)), 'w', 2*ones(size(x))));
add_ser_func_w = @(ser, name, x, T, type, w) ser.addSeries(name, struct('x', x, 'T', T, 'type', type*ones(size(x)), 'w', w*ones(size(x))));
ser = add_ser_func_w(ser, 'Spe02', Spe02_x, Spe02_T, 2, 1);
ser = add_ser_func(ser, 'Pin25', Pin25_x, Pin25_T, 2);
ser = add_ser_func(ser, 'Cha38', Cha38_x, Cha38_T, 1);
ser = add_ser_func(ser, 'Jan30', Jan30_x, Jan30_T, 2);
ser = add_ser_func(ser, 'Mil34', Mil34_x, Mil34_T, 2);
ser = add_ser_func(ser, 'Shn32', Shn32_x, Shn32_T, 2);
ser = add_ser_func(ser, 'Kak41', Kak41_x, Kak41_T, 2);
ser = add_ser_func(ser, 'Bab10', Bab10_x, Bab10_T, 1);
ser = add_ser_func(ser, 'Bab10_eut', Bab10_eut_x, Bab10_eut_T, 3);
ser = add_ser_func(ser, 'Cha38_eut', Cha38_eut_x, Cha38_eut_T, 3);
ser = add_ser_func(ser, 'Lee72', Lee72_x, Lee72_T, 2);
ser = add_ser_func(ser, 'Points', Points_x, Points_T, 2);
ser = ser.finalize;

ser = ser.cfgFigAxes('$T$, K', @(ser) ser.T, ...
    '$\left(T_{ij}^\mathrm{exp} - T_{ij}^\mathrm{calc}\right) / T_{ij}^\mathrm{exp} \cdot 100$', @(ser, res) res * 100);

    function Tcalc = sle_func(g00, ser)
        leq_func = @(x, T) ( H2Oprop.dmG_func(T) + td_func(g00, x, T, 'dmu1'));        
        req_func = @(x, T) ( URprop.dmG_func(T) + td_func(g00, x, T, 'dmu2')); 
        eut_func = @(a) [leq_func(a(1), a(2)); req_func(a(1), a(2))];
        
        Tcalc = nan(size(ser.type));
        for t = 1:3
            ii = (ser.type == t);            
            if sum(ii) > 0
                xv = ser.x(ii);
                Tv = ser.T(ii);
                switch t
                    case 1
                        Tcalc(ii) = liquidus_solver(xv, Tv, leq_func);                    
                    case 2
                        Tcalc(ii) = liquidus_solver(xv, Tv, req_func);
                    case 3
                        fopt = optimset('Display', 'off');
                        xT = fsolve(eut_func, [0.1 280], fopt);
                        Teut_calc = xT(2);
                        xeut_calc = xT(1);
                        Tcalc(ii) = Teut_calc;                        
                end
            end
        end
    end
end

function ser = load_phi_data
global H2Oprop
% Returns experimental data about osmotic coefficients in the UR-H2O system

R = getR();
Mw = H2Oprop.M; % M(H2O), g/mol


%----- PREPARE STRUCTURES -----
s = struct('T', [], 'x', [], 'phi', [], 'marker', []);
dat = struct('Sca38',s, 'Ell66',s, 'Cal97',s, 'deAze84',s, 'Bow63',s, 'Jak81', s, 'Per26', s);

%----- ISOPIESTIC DATA -----
% Water activity data
% Scatchard, G., Hamer, W.J., Wood, S.E. Isotonic solutions. I.
% The chemical potential of water in aqueous solutions of sodium chloride,
% potassium chloride, sulfuric acid, sucrose, urea and glycerol at 25C //
% // JACS, 1938, 60 (12), pp. 3061-3070
Scatchard38_dat = [ % Isopiestic experiment: mNaCl vs mUREA (25C)
    6.0781  20.007
    5.4593  17.087
    4.6013  13.332
    4.2585  11.957
    4.1983  11.750
    3.7390  9.9882
    3.3911  8.7450
    2.5022  5.8919
    2.4135  5.6262
    2.0845  4.6999
    1.6220  3.4728
    1.1614  2.3669
    1.0010  2.0088
    0.99954 2.0033
    0.68788 1.3375
    0.51709 0.99133
    0.50593 0.96818
    0.40846 0.77579
    0.40034 0.75828
    0.30922 0.58361
    0.30303 0.57270
    0.21275 0.40016
    0.20603 0.38766
    0.20432 0.38563
    0.20120 0.37746
    0.10288 0.19448
    0.10014 0.18866
    0.09930 0.18764
    0.09918 0.18845    
    ];

% Water activity/osmotic coefficient data
% Ellerton H.D., Dunlop P.J. Activity coefficients for the systems
% water-urea and water-urea-sucrose at 25C from isopiestic measurements //
% // J. Phys. Chem. 1966. V.70. N 6. P. 1831-1837.
Ellerton1966_dat = [ % m1, phi1, mNaCl
    0.2054  0.9909  0.1093 % Part 1
    0.2101  0.9895  0.1117
    0.4071  0.9835  0.2168
    0.4137  0.9838  0.2204
    0.6100  0.9765  0.3234
    0.6192  0.9763  0.3282
    0.8094  0.9682  0.4258
    0.8365  0.9658  0.4397
    1.0280  0.9613  0.5361
    1.0372  0.9607  0.5405
    1.4238  0.9493  0.7294
    1.4374  0.9486  0.7357
    1.9905  0.9333  0.9932
    2.0015  0.9345  0.9997
    2.4309  0.9228  1.1901
    2.4509  0.9220  1.1984
    2.4848  0.9206  1.2125
    2.5035  0.9202  1.2207 % Part 2
    2.9434  0.9105  1.4081
    2.9922  0.9099  1.4290
    3.8354  0.8937  1.7660
    3.8590  0.8931  1.7748
    4.9354  0.8757  2.1754
    4.9638  0.8757  2.1865
    5.3814  0.8691  2.3322
    5.4766  0.8676  2.3648
    5.8135  0.8634  2.4805
    5.8638  0.8627  2.4973
    6.0428  0.8601  2.5564
    6.0930  0.8596  2.5737
    6.3962  0.8557  2.6725
    6.4421  0.8555  2.6883
    6.8203  0.8505  2.8086
    6.8760  0.8502  2.8272
    7.1813  0.8469  2.9233 % Part 3
    7.2449  0.8460  2.9426
    7.9257  0.8387  3.1496
    8.0696  0.8374  3.1930
    9.8320  0.8236  3.7008
    9.9048  0.8235  3.7223
    12.0781 0.8093  4.2955
    12.1676 0.8088  4.3181
    14.0050 0.7991  4.7678
    14.1703 0.7981  4.8061
    15.6122 0.7905  5.1340
    16.0436 0.7886  5.2299
    18.1145 0.7819  5.6852
    18.3441 0.7802  5.7288
    19.8828 0.7753  6.0482
    20.0270 0.7750  6.0783 % Saturated solution
    ];

% Bower V.E., Robinson R.A. The thermodynamics of the ternary system:
% urea-sodium chloride-water at 25C // J. Phys. Chem. 1963, V.67(7), P.1524-1527.
Bower1963_dat = [ %mUR, mNaCl
    1.0257  0.5329
    1.0395  0.5384
    1.6247  0.8251
    3.6345  1.6876
    3.7667  1.7391
    6.9657  2.8575
    8.1817  3.2362
    10.427  3.8430
    11.058  4.0304
    11.184  4.0580
    14.008  4.7792
    14.306  4.8323
    17.826  5.6440
    18.401  5.7660
    ];

% ----- EBULIOMETRIC DATA -----
% Water osmotic coefficient data
% Calin S., Telea C., Chilom, G. A thermodynamic study of urea-water
% mixture by VPO // Rev. Roum. Chim. 1997. 42(2). P.85-92.
Calin1997_dat = [ %m(UR), phi(303K), phi(313K) phi(323K)
    0.2     0.992162    0.993002    0.993782    
    0.3     0.988243    0.989503    0.990670
    0.4     0.984324    0.986004    0.987561
    0.5     0.980405    0.982505    0.984456
    0.6     0.976486    0.979006    0.981342
    0.7     0.972567    0.975507    0.978235
    0.8     0.968468    0.972008    0.975123
    1.0     0.960810    0.965010    0.968910
    ];
Calin1997_T_vals = [303, 313, 323];

% de Azevedo F.G., de Oliver W.A. Ebuliometric behaviour of aqueous
% solutions of urea // J. Chem. Thermodynamics. 1984. 16. p.683-686.
deAzevedo1984_dat = [ % mUR, dT, phi
    0.0436  0.0221  0.9987 % First column
    0.0598  0.0303  0.9983
    0.0835  0.0423  0.9981
    0.0920  0.0466  0.9980
    0.1141  0.0578  0.9980
    0.1234  0.0625  0.9978
    0.1337  0.0677  0.9976
    0.1422  0.0720  0.9975
    0.1505  0.0762  0.9974
    0.1652  0.0836  0.9969
    0.1815  0.0918  0.9964 
    0.1970  0.0996  0.9959 % Second column
    0.2078  0.1050  0.9953
    0.2135  0.1078  0.9946
    0.2473  0.1248  0.9940
    0.2680  0.1352  0.9936
    0.2809  0.1416  0.9929
    0.2903  0.1463  0.9926
    0.3021  0.1522  0.9923
    0.3256  0.1640  0.9920
    0.3484  0.1754  0.9915
    0.3772  0.1899  0.9914
    ];
% ----- VAPOUR PRESSURE DATA -----
% Jakli, Gy., Van Hook W. A. Isotope effects in aqueous systems. 12.
% thermodynamics of urea-h4/H2O an urea-d4/D2O solutions // 
% // J. Phys. Chem, 1981, 85(23), 3480-3493
% DOI: 10.1021/j150623a025
Jakli1981_dat = [ %m tC phi
    4.00114     6.64    0.867
    4.00114     10.90   0.871
    4.00114     15.15   0.877
    4.00114     19.34   0.883
    4.00114     23.54   0.889
    4.00114     27.69   0.894
    4.00114     19.94   0.886
    4.00114     24.09   0.891
    4.00114     28.10   0.895
    4.00114     32.06   0.899
    4.00114     36.04   0.904
    4.00114     39.95   0.906
    4.00114     44.04   0.909
    4.00114     48.07   0.914
    4.00114     52.08   0.914
    4.00114     56.05   0.914
    
    8.0040      7.65    0.812
    8.0040      18.08   0.825
    8.0040      26.42   0.838
    8.0040      30.37   0.841
    8.0040      34.23   0.845
    8.0040      38.15   0.850
    8.0040      42.32   0.854
    8.0040      46.50   0.857
    8.0040      50.63   0.860
    8.0040      54.94   0.861
    8.0040      59.11   0.863
    8.0040      63.17   0.859
    
    11.9859     9.75    0.784
    11.9859     13.79   0.791
    11.9859     17.99   0.799
    11.9859     22.22   0.805
    11.9859     26.36   0.811
    11.9859     30.58   0.816
    11.9859     34.66   0.820
    11.9859     34.66   0.819
    11.9859     38.91   0.824
    11.9859     43.08   0.828
    11.9859     47.09   0.831
    11.9859     51.31   0.833
    11.9859     55.47   0.834

    15.9774     33.43   0.803
    15.9774     37.90   0.806
    15.9774     42.02   0.806
    15.9774     46.35   0.807
    15.9774     50.49   0.808
    15.9774     54.47   0.812
    15.9774     58.54   0.812
    15.9774     60.43   0.811

    15.9978     29.45   0.797
    15.9978     33.83   0.801
    15.9978     38.13   0.805
    15.9978     42.59   0.807
    15.9978     47.04   0.809
    
    19.9932     29.91   0.779
    19.9932     31.99   0.779
    19.9932     36.04   0.783
    19.9932     40.31   0.786
    19.9932     44.54   0.788
    19.9932     48.78   0.790
    19.9932     52.94   0.793
    19.9932     57.04   0.794
    19.9932     61.09   0.795
    ];

% Grollman A. The vapour pressure of aqueous solutions with special
% reference to the problem of the state of water in biological fluids //
% // The Journal of General Physiology. 1931. V.14. N.5. P.661-683.
Grollman1931_dat = [ % tC, mUR, (p0 - p)/(m*p0)
    20.3    0.05    1.66e-2
    20.3    0.10    1.64e-2
    20.3    0.15    1.65e-2
    20.3    0.20    1.63e-2
    20.3    0.30    1.63e-2
    20.3    0.40    1.65e-2
    20.3    0.50    1.64e-2
    ];

% To E.C.H., Hu J., Haynes C.A., Koga Y. Interactions in 1-propanol - urea
% - H2O: chemical potentials, partial molar enthalpies and entropies at 25C
% // J. Phys. Chem. B. 1998. V.102. N.52. P.10958-10965.
To1998_dat = [ %xUR, pmmHg
    25      0.0000  23.756
    25      0.0306  23.071
    25      0.0441  22.784
    25      0.0746  22.138
    25      0.1007  21.571
    25      0.1921  19.745
    ];

% Perman E.P., Lovett T. Vapour pressure and heat of dilution of aqueous
% solutions // Trans. Faraday Soc. 1926. 22. 1-19.
% (they have systematic error, excluded from the optimization)
Per1926_dat = [%t wUR, pmmHg pmmHg(0)
    40.02   17.42   52.8    55.22
    40.02   48.54   45.0    55.22
    40.02   69.15   34.65   55.22
    40.02   67.17   36.5    55.22
    
    49.99   7.364   90.45   92.35
    49.99   12.49   88.87   92.35
    49.99   26.17   84.57   92.35
    49.99   36.90   79.85   92.35
    49.99   47.89   74.5    92.35
    49.99   51.81   72.65   92.35
    49.99   62.33   64.70   92.35
    49.99   64.36   62.60   92.35
    49.99   66.92   59.8    92.35
    
    60.28   7.148   149.1   151.42
    60.28   12.93   146.8   151.42
    60.28   18.70   144.7   151.42
    60.28   21.29   142.2   151.42
    60.28   30.86   136.1   151.42
    60.28   37.01   131.3   151.42
    60.28   45.82   123.8   151.42
    60.28   53.19   118.0   151.42
    60.28   58.03   111.7   151.42
    60.28   61.08   107.0   151.42
    60.28   63.36   104.0   151.42
    60.28   68.36   95.4    151.42
    60.28   73.05   85.5    151.42
    
    70.39   9.063   234.65  237.8
    70.39   9.90    233.4   237.8
    70.39   14.17   231.8   237.8
    70.39   14.83   230.1   237.8
    70.39   24.30   222.4   237.8
    70.39   27.79   217.5   237.8
    70.39   31.89   213.1   237.8
    70.39   35.39   209.0   237.8
    70.39   41.58   200.0   237.8
    70.39   49.67   190.1   237.8
    70.39   50.27   189.4   237.8
    70.39   53.86   183.1   237.8
    70.39   56.57   176.6   237.8
    70.39   65.44   157.3   237.8
    70.39   74.67   132.2   237.8
    
    80.10   13.05   351.3   356.8
    80.10   24.84   338.4   356.8
    80.10   28.38   328.85  356.8
    80.10   35.16   316.0   356.8
    80.10   39.29   307.5   356.8
    80.10   44.80   298.7   356.8
    80.10   45.33   292.2   356.8
    80.10   47.39   287.5   356.8
    80.10   50.42   281.5   356.8
    80.10   56.70   265.0   356.8
    80.10   64.97   237.9   356.8
    80.10   68.47   222.95  356.8
    80.10   74.03   198.3   356.8
    80.10   77.64   175.6   356.8
    80.10   79.79   165.7   356.8
    80.10   80.60   161.2   356.8
    ];

% Transform raw data into osmotic coefficients
[Sca38_NaCl_phi,~,Sca38_p] = NaCl_phi_298(Scatchard38_dat(:,1));
Sca38_m = Scatchard38_dat(:,2);
dat.Sca38.x = UW_mtox(Sca38_m); % x(UR)
dat.Sca38.T = (25 + 273.15) * ones(size(dat.Sca38.x));
dat.Sca38.phi = Sca38_NaCl_phi .* (2 * Scatchard38_dat(:,1)) ./ Sca38_m;
dat.Sca38.marker = 'b*';

Ell66_m = Ellerton1966_dat(:,1);
dat.Ell66.x = UW_mtox(Ell66_m);
[Ell66_NaCl_phi,~,Ell66_p] = NaCl_phi_298(Ellerton1966_dat(:,3));
dat.Ell66.T = (25 + 273.15) * ones(size(Ell66_p));
dat.Ell66.phi = Ell66_NaCl_phi .* (2 * Ellerton1966_dat(:,3)) ./ (Ell66_m);
dat.Ell66.marker = 'bd';

Bow63_m = Bower1963_dat(:,1);
Bow63_mNaCl = Bower1963_dat(:,2);
dat.Bow63.x = UW_mtox(Bow63_m);
Bow63_NaCl_phi = NaCl_phi_298(Bow63_mNaCl);
dat.Bow63.T = (25 + 273.15) * ones(size(Bow63_m));
dat.Bow63.phi = Bow63_NaCl_phi .* (2 * Bow63_mNaCl)./ Bow63_m;
dat.Bow63.marker = 'bo';

dat.Cal97.x = repmat(UW_mtox(Calin1997_dat(:,1)), size(Calin1997_dat, 2) - 1, 1);
dat.Cal97.T = repmat(Calin1997_T_vals, size(Calin1997_dat, 1), 1);
dat.Cal97.T = dat.Cal97.T(:);  
dat.Cal97.phi = Calin1997_dat(:, 2:end);
dat.Cal97.phi = dat.Cal97.phi(:);
dat.Cal97.marker = 'r.';

dat.deAze84.x = UW_mtox(deAzevedo1984_dat(:,1));
dat.deAze84.T = 371.5 + deAzevedo1984_dat(:,2);
dat.deAze84.phi = deAzevedo1984_dat(:,3);
dat.deAze84.marker = 'rx';

dat.Jak81.x = UW_mtox(Jakli1981_dat(:,1));
dat.Jak81.T = Jakli1981_dat(:,2) + 273.15;
dat.Jak81.phi = Jakli1981_dat(:,3);
dat.Jak81.marker = 'kv';

dat.Gro31.T = Grollman1931_dat(:,1) + 273.15;
Gro31_m = Grollman1931_dat(:,2);
dat.Gro31.x = UW_mtox(Gro31_m);
%p0 = exp(lnp0_H2O(dat.Gro31.T));
dat.Gro31.a = 1 - (Grollman1931_dat(:,3) .* Gro31_m);
dat.Gro31.phi = -log(dat.Gro31.a) ./ (Mw * 1e-3 * Gro31_m);
dat.Gro31.marker = 'k+';

dat.To98.T = To1998_dat(2:end,1) + 273.15;
dat.To98.x = To1998_dat(2:end,2);
dat.To98.a = To1998_dat(2:end,3) ./ To1998_dat(1,3);
dat.To98.phi = -log(dat.To98.a) ./ (Mw * 1e-3 * UW_xtom(dat.To98.x));
dat.To98.marker = 'ks';

dat.Per26.T = Per1926_dat(:,1) + 273.15;
dat.Per26.x = UW_wtox(Per1926_dat(:,2));
dat.Per26.a = Per1926_dat(:,3)./Per1926_dat(:,4);
dat.Per26.phi = -log(dat.Per26.a) ./ (Mw * 1e-3 * UW_xtom(dat.Per26.x));
dat.Per26.marker = 'mh';


mode = 'a';
ser = [];
switch mode
    case 'phi'
        phi_func = @(g00, ser) -td_func(g00, ser.x, ser.T, 'dmu1')./(UW_xtom(ser.x)*Mw*1e-3)./ (R*ser.T);
        ser = DataGroup('phi', {'phi', 'x', 'T', 'marker'}, phi_func, 'abs');        
        ytext = '$\left(\phi^\mathrm{exp} - \phi^\mathrm{calc}\right)\cdot{100}$';
    case 'a'
        fields = fieldnames(dat);
        for i = 1:numel(fields)
            fname = fields{i};
            ser = dat.(fname);
            dat.(fname).a = exp(-ser.phi.*UW_xtom(ser.x).*Mw*1e-3 );
        end        
        a_func = @(g00, ser) exp(td_func(g00, ser.x, ser.T, 'dmu1')./ (R*ser.T));
        ser = DataGroup('a', {'a', 'x', 'T', 'marker', 'phi'}, a_func, 'abs');        
        ytext = '$\left(a_1^\mathrm{exp} - a_1^\mathrm{calc}\right)\cdot{100}$';        
end

ser = ser.addSeries('Sca38', dat.Sca38);
ser = ser.addSeries('Ell66', dat.Ell66);
ser = ser.addSeries('Bow63', dat.Bow63);
ser = ser.addSeries('Jak81', dat.Jak81);
ser = ser.addSeries('deAze84', dat.deAze84);
ser = ser.addSeries('Cal97', dat.Cal97);
ser = ser.addSeries('Gro31', dat.Gro31);
ser = ser.addSeries('To98', dat.To98);
%ser = ser.addSeries('Per26', dat.Per26); % Perman1926 excluded!
ser = ser.finalize();
ser = ser.cfgFigAxes('$x\left(\mathrm{(NH_2)_2CO}\right)$', @(ser) ser.x, ...
    ytext, @(ser, res) res * 100);
end

function ser = load_dHdil_data
% Loads enthalpies of dilution

% Hamilton D., Stokes R.H. Enthalpies of dilution of urea solution in six
% polar solvents at several temperatures // J. Solution Chemistry, 1972,
% V 1, N 3, P.223-235.
Hamilton1972_dat = [ % tC m1 m2 dH
% At 5C, m1 = 7.943    
    5   7.943   0.1176  2310.4
    5   7.943   0.2381  2262.4
    5   7.943   0.4563  2152.3
    5   7.943   0.6848  2043.3
    5   7.943   1.0409  1882.3
    5   7.943   1.3342  1756.5
    5   7.943   1.6046  1647.5
    5   7.943   1.9022  1532.5
    5   7.943   2.1637  1436.2
    5   7.943   2.3561  1368.3
    5   7.943   2.5850  1290.3
    5   7.943   2.8030  1218.1
    5   7.943   2.9882  1158.4
    5   7.943   3.1418  1110.3
    5   7.943   3.3324  1052.1
    5   7.943   3.3695  1041.1
    5   7.943   7.5335  73.2
    5   7.943   7.1293  148.3
    5   7.943   6.5053  270.8
    5   7.943   5.9702  382.8
    5   7.943   5.4922  488.5
    5   7.943   5.1548  567.2
    5   7.943   4.8021  653.0
    5   7.943   4.5044  728.1
    5   7.943   4.2348  799.1
    5   7.943   3.9983  863.2
    5   7.943   3.8078  916.5
    5   7.943   3.6073  974.3
    5   7.943   3.4406  1023.5
% At 45C, m1 = 9.512
    45  9.512   0.1485  1517.2
    45  9.512   0.2728  1479.8
    45  9.512   0.5169  1412.5
    45  9.512   0.9773  1294.0
    45  9.512   1.3900  1197.2
    45  9.512   1.7733  1111.4
    45  9.512   2.1165  1039.8
    45  9.512   2.4393  975.0
    45  9.512   2.7272  919.2
    45  9.512   2.9937  869.3
    45  9.512   3.2518  822.4
    45  9.512   3.4675  784.3
    45  9.512   3.6842  746.8
    45  9.512   3.8837  713.4
    45  9.512   8.5166  100.5
    45  9.512   7.6939  191.3
    45  9.512   6.9926  274.1
    45  9.512   6.4476  342.0
    45  9.512   5.9559  404.8
    45  9.512   5.5544  459.7
    45  9.512   5.1957  510.8
    45  9.512   4.8811  557.2
    45  9.512   4.5954  600.7
    45  9.512   4.3523  638.4
    45  9.512   4.1281  674.3
    45  9.512   3.9248  707.5
% At 25C, m1 = 13.599
    25  13.599  12.8005 70.7
    25  13.599  11.2597 220.4
    25  13.599  10.2142 332.3
    25  13.599  9.2495  445.3
    25  13.599  8.6220  524.3
    25  13.599  7.9502  613.9
    25  13.599  7.3591  697.3
    25  13.599  6.8888  766.9
    25  13.599  6.3094  857.4
    25  13.599  5.7773  945.5
    25  13.599  5.4526  1001.5
    25  13.599  5.1977  1046.7
% At 25C, m1 = 8.085
    25  8.085   0.1162  1710.4
    25  8.085   0.2240  1677.8
    25  8.085   0.4365  1603.1
    25  8.085   0.8298  1477.5
    25  8.085   1.0270  1416.5
    25  8.085   1.3503  1322.8
    25  8.085   1.5072  1278.2
    25  8.085   1.7991  1199.2
    25  8.085   2.0670  1129.2
    25  8.085   2.3133  1066.8
    25  8.085   2.5398  1010.9
    25  8.085   2.7507  960.5
    25  8.085   2.9468  914.3
    25  8.085   3.1260  873.2
    25  8.085   3.1781  861.3
    25  8.085   7.8784  27.7
    25  8.085   7.6795  55.4
    25  8.085   7.3100  108.2
    25  8.085   6.6693  203.9
    25  8.085   6.1271  289.7
    25  8.085   5.6711  365.9
    25  8.085   5.2728  435.9
    25  8.085   4.9298  498.5
    25  8.085   4.6248  556.1
    25  8.085   4.3664  606.5
    25  8.085   4.1258  654.7
    25  8.085   3.9172  697.5
    25  8.085   3.7255  737.8
% At 25C, m1 = 2.7094
    25  2.7094  0.1075  744.9
    25  2.7094  0.1782  726.0
    25  2.7094  0.3329  671.6
    25  2.7094  0.4697  625.6
    25  2.7094  0.5905  586.4
    25  2.7094  0.6996  551.5
    25  2.7094  0.8066  518.9
    25  2.7094  0.8894  493.4
    25  2.7094  0.9732  468.2
    25  2.7094  1.0494  446.0
    25  2.7094  1.1156  426.8
    25  2.7094  1.1788  408.9
    25  2.7094  1.1899  406.0
    ];

% Gucker F.T., Jr., Pickard H.B. The heats of dilution, heat capacities
% and activities of urea in aqueous solution from the freezing point to
% 40C // JACS. 1940. V.62. N.6. P.1464-1472.
Gucker40_dat = [ % tC, m1, m2, dH(cal15/mol); cal15= 4.1855J
    25  0.2000  0.0050  15.8
    25  0.2000  0.0050  16.8
    25  0.2000  0.0050  15.8
    25  0.2000  0.0050  17.2
    25  0.2000  0.0100  15.8
    25  0.2000  0.0100  15.8
    25  3.007   0.0100  207.1
    25  3.007   0.0100  206.7
    25  6.002   0.0100  349.7
    25  6.002   0.0100  347.0
    25  8.010   0.0100  424.3
    25  8.010   0.0100  424.8
    25  10.003  0.0100  484.4
    25  10.003  0.0100  482.5
    25  11.992  0.0100  532.2
    25  11.992  0.0100  532.7
    ];

% Naud\'{e} S.M. // Z.Phys.Chem. 1928. V.134. N3/4. P.209-236.
Naude1928_dat = [ %c1(mol/l) c2     -dH(per10cm3) -dH(permolUR), Ndots, tC
    1       0.02    -0.930  -93.0   4   17.88
    1       0.04    -0.875  -87.5   4   17.88
    0.5     0.01    -0.225  -44.9   4   18.31
    0.333   0.0067  -0.098  -28.5   2   18.51
    0.333   0.0133  -0.092  -27.5   2   18.51
    0.1     0.003   -0.014  -14     2   18.10
    0.033   0.001   -0.003  -8      2   18.21
    0.01    0.0003  -0.0003 -3      2   18.30
    ];

% Zhu L.-Y., Hu X.-G., Wang, H.-Q., Chen N. Enthalpic pairwise 
% self-interactions of urea and its four derivatives in (dimethylformamide
% + water) mixtures rich in water at 298.15 K // J. Chem. Thermodynamics,
% 2016, 93, 200-204.
%http://dx.doi.org/10.1016/j.jct.2015.10.010
Zhu2016_dat = [ %m0 = 0.6066; T, m1, m2, J/(mol*K) (per urea in Vinj)
    298.15	0.00721	0.01315	198.26
    298.15	0.01315	0.01903	194.68
    298.15	0.01903	0.02485	188.06
    298.15	0.02485	0.03061	183.82
    298.15	0.03061	0.03630	179.82
    298.15	0.03630	0.04194	175.46
    298.15	0.04194	0.04752	171.14
    298.15	0.04752	0.05304	169.41
    298.15  0.05304	0.05850	164.75
    298.15	0.05850	0.06390	160.36
    298.15	0.06390	0.06924	158.65
    298.15	0.06924	0.07451	154.58
    298.15	0.07451	0.07973	150.51
    298.15	0.07973	0.08489	148.20
    298.15	0.08489	0.08999	145.72
    298.15	0.08999	0.09502	141.29
    298.15	0.09502	0.10000	138.80
    298.15	0.10000	0.10492	136.50    
    ];



% Fill output structure with data series
% LIQ(x2_0) + LIQ(x2_1) = LIQ(x2_2)
s = struct('dH', [], 'x2_0', [], 'x2_1', [], 'x2_2', [], 'T', [], 'w', [], 'marker', []);
dat = struct('Ham72',s, 'Guc40',s, 'Nau28',s, 'Zhu16', s);

dat.Ham72.T = Hamilton1972_dat(:,1) + 273.15;
dat.Ham72.x2_0 = UW_mtox(Hamilton1972_dat(:,2));
dat.Ham72.x2_1 = zeros(size(dat.Ham72.T));
dat.Ham72.x2_2 = UW_mtox(Hamilton1972_dat(:,3));
dat.Ham72.dH = Hamilton1972_dat(:,4);
dat.Ham72.w = 1e-3*ones(size(dat.Ham72.T));
dat.Ham72.marker = 'bs';

dat.Guc40.T = Gucker40_dat(:,1) + 273.15;
dat.Guc40.x2_0 = UW_mtox(Gucker40_dat(:,2));
dat.Guc40.x2_1 = zeros(size(dat.Guc40.T));
dat.Guc40.x2_2 = UW_mtox(Gucker40_dat(:,3));
dat.Guc40.dH = Gucker40_dat(:,4) * 4.1855; % CAL15!
dat.Guc40.w = 1e-3*ones(size(dat.Guc40.T));
dat.Guc40.marker = 'bd';

dat.Nau28.T = Naude1928_dat(:,6) + 273.15;
dat.Nau28.x2_0 = UW_mtox(Naude1928_dat(:,1));
dat.Nau28.x2_1 = zeros(size(dat.Nau28.T));
dat.Nau28.x2_2 = UW_mtox(Naude1928_dat(:,2));
dat.Nau28.dH = -Naude1928_dat(:,4) * 4.184;
dat.Nau28.w = (1/500)*ones(size(dat.Nau28.T));
dat.Nau28.marker = 'bo';

dat.Zhu16.T = Zhu2016_dat(:,1)';
dat.Zhu16.x2_0 = UW_mtox(0.6006*ones(size(dat.Zhu16.T)));
dat.Zhu16.x2_1 = UW_mtox(Zhu2016_dat(:,2)');
dat.Zhu16.x2_2 = UW_mtox(Zhu2016_dat(:,3)');
dat.Zhu16.dH = Zhu2016_dat(:,4)';
dat.Zhu16.w = (1/500)*ones(size(dat.Zhu16.T));
dat.Zhu16.marker = 'bx';

dHdil_func = @(g00, ser) (...
        td_func(g00, ser.x2_2, ser.T, 'dmixH') .* (ser.x2_1 - ser.x2_0)./(ser.x2_1 - ser.x2_2) - ...
        td_func(g00, ser.x2_1, ser.T, 'dmixH') .* (ser.x2_2 - ser.x2_0)./(ser.x2_1 - ser.x2_2) - ...
        td_func(g00, ser.x2_0, ser.T, 'dmixH') ...
    ) ./ ser.x2_0;

ser = DataGroup('dHdil', {'dH', 'x2_0', 'x2_1', 'x2_2', 'T', 'marker'}, dHdil_func, 'abs');
ser = ser.addSeries('Nau28', dat.Nau28);
ser = ser.addSeries('Guc40', dat.Guc40);
ser = ser.addSeries('Ham72', dat.Ham72);
ser = ser.addSeries('Zhu16', dat.Zhu16);
ser = ser.finalize();
ser = ser.cfgFigAxes('$\Delta_\mathrm{dil}H~/~\mathrm{kJ}\cdot\mathrm{mol}^{-1}$', @(ser) ser.dH * 1e-3, ...
    '$\Delta_\mathrm{dil}H^\mathrm{exp} - \Delta_\mathrm{dil}H^\mathrm{calc}~/~\mathrm{J}\cdot\mathrm{mol}^{-1}$', @(ser, res) (res) ); %!!!!!!!!!!!!!!!!!!
end

function ser = load_dHw_data
% Loads partial enthalpies of water

% Gucker F.T., Jr., Pickard H.B. The heats of dilution, heat capacities
% and activities of urea in aqueous solution from the freezing point to
% 40C // JACS. 1940. V.62. N.6. P.1464-1472.
Gucker40_part_dH_dat = [ % tC, m1, m2, dH(cal15/mol); cal15= 4.1855J
    25  0.1420  0.1340  0.66
    25  0.1500  0.1420  0.66
    25  0.1500  0.1420  0.68
    25  0.2000  0.1900  0.83
    25  0.2100  0.2000  0.83
    25  0.2100  0.2000  0.82
    25  0.3100  0.3000  0.829
    25  0.3100  0.3000  0.830
    25  0.4100  0.4000  0.807
    25  0.4100  0.4000  0.814
    25  0.7000  0.6900  0.786
    25  0.7001  0.6901  0.783
    25  1.0100  1.0000  0.732
    25  1.0154  1.0053  0.746
    25  1.4997  1.4900  0.663
    25  1.4997  1.4900  0.662
    25  2.0100  2.0000  0.625
    25  2.0100  2.0000  0.631
    25  2.5100  2.5000  0.584
    25  2.5100  2.5000  0.586
    25  3.0100  3.0000  0.545
    25  3.0100  3.0000  0.545    
    ];

% x) partial enthalpies
T = Gucker40_part_dH_dat(:,1)' + 273.15;
x2_1 = UW_mtox(Gucker40_part_dH_dat(:,2))';
x2_2 = UW_mtox(Gucker40_part_dH_dat(:,3))';
x = 0.5 * (x2_1 + x2_2);

nw2 = (1 - x2_2)./x2_2;
nw1 = (1 - x2_1)./x2_1;
dHw = Gucker40_part_dH_dat(:,4)' * 4.1855 ./ (nw2 - nw1); % CAL15!

dH1_func = @(a, dat) td_func(a, dat.x, dat.T, 'dH1');
ser = DataGroup('dH1', {'dH', 'x', 'T'}, dH1_func, 'mean');
ser = ser.addSeries('Guc40', struct('dH', dHw, 'x', x, 'T', T ));
ser = ser.finalize;
ser = ser.cfgFigAxes('$x$', @(ser) ser.x, ...
    '$\left(\Delta\bar{H}_1^\mathrm{exp} - \Delta\bar{H}_1^\mathrm{calc}\right) / \overline{\Delta\bar{H}_1^\mathrm{calc}} \cdot 100$', ...
    @(ser, res) (res * 100));
end

function ser = load_dHur_dat
global URprop
% Partial molar enthalpy of solution of SOLID urea in water-urea solution

% Koga Y., Miyazaki Y., Nagano Y., Inaba A. Mixing schemes in a urea-H2O
% system: a differential approach in solution thermodynamics // J. Phys.
% Chem. B. 2008. 112. 11341-11346.
% Raw data are taken from supplementary materials (XLS file)
Koga2008_dat = [ % xUR, dHsol(kJ/mol)
    0.000637652	15.23
    0.001866231	15.17
    0.003119083	15.12
%    0.01078     14.19 % Inaccurate data
    0.01194     14.78
    0.01320     14.78
    0.02120     14.49
%    0.02805     13.98 % Inaccurate data
    0.02935     14.27
    0.03606     14.02
    0.04419     13.86
    0.05105     13.67
    0.05170     13.57
    0.05828     13.52
    0.05892     13.49
    0.06594     13.38
    0.06650     13.35
    0.07508     13.15
    0.08401     13.01
    0.09562     12.80
    0.09622     12.78
    0.1075      12.62
    0.1080      12.58
    0.1122      12.54
    0.1223      12.32
    0.1285      12.35
    0.1336      12.21
    0.1427      12.12
    0.1508      12.00
    0.1640      11.92
    0.1711      11.75
    0.1773      11.80
    0.1840      11.67
    0.1901      11.65
    0.1966      11.62
    0.2025      11.61
    0.2093      11.53
    0.2155      11.53
    0.2224      11.40
    0.2287      11.43
    0.2333      11.50
%    0.2403      10.89 % Inaccurate data
%    0.2431      11.19
%    0.2436      12.79
%    0.2442      11.51
%    0.2454      11.45
];

% x) partial enthalpies
x = Koga2008_dat(:,1)';
T = 298.15 * ones(size(x));
dHur = Koga2008_dat(:,2)' * 1000; % kJ to J

dH2_func = @(g00, ser) URprop.dmH_func(ser.T) + td_func(g00, ser.x, ser.T, 'dH2');
ser = DataGroup('dH2', {'dH', 'x', 'T'}, dH2_func, 'mean');
ser = ser.addSeries('Kog08', struct('dH', dHur, 'x', x, 'T', T));
ser = ser.finalize();
ser = ser.cfgFigAxes('$x\left(\mathrm{(NH_2)_2CO}\right)$', @(ser) ser.x, ...
    '$\left(h_2^\mathrm{exp} - h_2^\mathrm{calc}\right) / \overline{h_2^\mathrm{exp}} \cdot 100$', ...
    @(ser, res) (res * 100));
end


function ser = load_dHsol_inf_data
global URprop
% Load enthalpies of solution of solid urea for infinite dilution
% (taken from articles when there are no raw data for diluted solutions)

s = struct('dH', [], 'ddH', [], 'T', [], 'w', []);
dat = struct('deVis72', s, 'Sch75', s, 'Des76', s, 'Bur77', s, ...
    'Pru79', s, 'Abr80', s, 'Osi82', s, 'Pie86', s, 'Pie95', s);
weight = 2;

% de Visser C., Gr\:{u}nbauer, H.J.M., Somsen, G. Enthalpies of solution
% of urea in binary mixtures of water and some amides at 25C // Z. Phys.
% Chem. Neue Folge, 1972, 97(12), P.69-78.
dat.deVis72.dH  = 15280;
dat.deVis72.ddH =   300;
dat.deVis72.T   = 298.15;
dat.deVis72.w   = weight;

% UTF8 ENCODING
% Абросимов В.К., Лявданский В.В., Крестов Г.А. Термохимическое
% исследование растворов карбамида, гексаметилентетрамина и бромидов
% тетраметиламмония в D2O и H2O при 283-338 K // Известия высших учебных
% заведений. Химия и химическая технология. 1980. 23(10), 1251-1255.
dat.Abr80.dH  = [ 15862  15656  16263  17719];
dat.Abr80.ddH = [    20    100     40    100];
dat.Abr80.T   = [283.26 298.17 318.16 338.10];
dat.Abr80.w   = weight;

% Taniewska-Osinska S., Pa\lecz B. Enthalpies of solution of hydroxyurea in
% water in the temperature range 293.15 to 323.15 K // J. Chem.
% Thermodynamics. 1982. 14. 11-14.
dat.Osi82.dH = 15320;
dat.Osi82.ddH = 40;
dat.Osi82.T = 298.15;
dat.Osi82.w = weight;

% Schrler M.Y., Turner P.J., Schrier E.E. Thermodynamic quantities for the
% transfer of urea from water to aqueous electrolyte solutions //
% // J. Phys. Chem. 1975. 79(14). 1391-1396.
dat.Sch75.dH = 3654 * 4.184;
dat.Sch75.ddH = 1 * 4.184;
dat.Sch75.T = 298.15;
dat.Sch75.w = weight;

% Desnoyers J.E., Perron G., Av\'{e}dikan L., Morel J.-P. Enthalpies of the
% urea-tert-butanol-water system at 25C // Journal of Solution Chemistry,
% 1976, 5(9), 631-644.
dat.Des76.dH = 15300;
dat.Des76.ddH = 200;
dat.Des76.T = 298.15;
dat.Des76.w = weight;

% Piekarski H., Somsen G. Enthalpies of solution of urea in water-alkanol
% mixtures and the enthalpic pair interaction coefficients of urea and
% several nonelectrolytes in water // Can. J. Chem., 1986, 64, 1721-1724
%
dat.Pie86.dH = 15290;
dat.Pie86.ddH = 30;
dat.Pie86.T = 298.15;
dat.Pie86.w = weight;

% Piekarski H., Waliszewski D. Dissolution enthalpies of urea in aqueous
% soltions off alkoxyethanols at 298.15 K // Thermochimica acta, 1995,
% 258, 67-76.
% http://dx.doi.org/10.1016/0040-6031(94)02238-J
dat.Pie95.dH = 15340;
dat.Pie95.ddH =  20;
dat.Pie95.T = 298.15;
dat.Pie95.w = weight;

% Bury, R.; Mayaffre, A.; Chemla, M. Enthalpies de solution de l’ure\'{e}
% dans des syst\'{e}mes mixtes hydroorganique et electrolitiques. J. Chim.
% Phys. 1977, 74, 746-750.
dat.Bur77.dH = 15310;
dat.Bur77.ddH = NaN;
dat.Bur77.T = 298.15;
dat.Bur77.w = weight;

% UTF8 Encoding
% Прусов А.Н., Захаров А.Г., Крестов Г.А. Термохимические исследования 
% \epsilon - аминокапроновой кислоты, мочевины и моногидрата глюкозы при 
% 25-55C // Проблемы сольватации и комплексообразования. Межвузовский
% сборник. Иваново, 1979.
dat.Pru79.dH  = [3680    3700    3723    3747] * 4.184;
dat.Pru79.ddH = [11      11      11      11] * 4.184;
dat.Pru79.T   = [25      35      45      55] + 273.15;
dat.Pru79.w   = weight*ones(1,4);

dmixH = @(g00, x, T) td_func(g00, x, T, 'dmixH');
dHdil_func = @(g00, x1, x2, T) dmixH(g00, x2, T) ./ x2 - dmixH(g00, x1, T) ./ x1;
dHsol_func = @(g00, ser) URprop.dmH_func(ser.T) + dHdil_func(g00, 1, 1e-6, ser.T);
ser = DataGroup('dHsol_inf', {'dH', 'ddH', 'T'}, dHsol_func, 'rel');
ser = ser.addSeries('deVis72', dat.deVis72);
ser = ser.addSeries('Sch75', dat.Sch75);
ser = ser.addSeries('Des76', dat.Des76);
ser = ser.addSeries('Bur77', dat.Bur77);
ser = ser.addSeries('Pru79', dat.Pru79);
%ser = ser.addSeries('Abr80', dat.Abr80);
ser = ser.addSeries('Osi82', dat.Osi82);
ser = ser.addSeries('Pie86', dat.Pie86);
ser = ser.addSeries('Pie95', dat.Pie95);

ser = ser.finalize();
ser = ser.cfgFigAxes('$T$, K', @(ser) (ser.T), ...
    '$\left( \Delta{H}_\mathrm{sol}^\mathrm{exp} - \Delta{H}_\mathrm{sol}^\mathrm{calc} \right) / \Delta{H}_\mathrm{sol}^\mathrm{exp} \cdot 100$', ...
    @(ser, res) (res * 100));


end

function ser = load_dHsol_data
global URprop
% Load enthalpies of solution of solid urea

% Subramanian, S., Balasubramanian, D., Ahluwalia, J.C. Thermochemical
% studies on the influence of urea on water structure // J. Phys. Chem.,
% 1969, 73(1), 266-269
Subramanian1969_dat = [ % tC, m*1000, dHsol(cal/mol)
    25  2.197   3730
    25  4.287   3688
    25  6.445   3690
    25  7.437   3725
    25  8.614   3669
    25  10.420  3697
    25  14.710  3642
    25  16.400  3645
    
    35  3.341   3773
    35  4.305   3721
    35  5.388   3761
    35  6.645   3740
    35  8.132   3753
    35  9.942   3670
    35  10.790  3679
    35  11.600  3707
    ];

% Subramanian, S., Sarma, T.S., Balasubramanian, D. Effects of the 
% urea-guanidinium class of protein denaturants on water structure: heats
% of solution and proton chemical shift studies // J. Phys. Chem., 1971,
% 75(6),815-820.
Subramanian1971_dat = [ %tC, m*1000, dHsol(cal/mol)
    9   1.98    3717
    9   2.15    3745
    9   2.31    3719
    9   2.48    3688
    9   2.62    3738
    9   4.42    3730
    9   4.57    3722
    9   6.65    3700
    9   7.80    3726
    
    15  2.72    3694
    15  2.99    3672
    15  3.18    3718
    15  3.31    3654
    15  4.43    3687
    15  4.71    3708
    15  10.07   3657
    ];

% Kustov, A.V., Smirnova, N.L. Standard enthalpies and heat capacities of
% solution of urea and tetramethylurea in water // J. Chem. Eng. Data.
% 2010, 55, 3055-3058.
Kustov2010_dat = [ % TK, m, dHsol (kJ/mol)
    278.15  0.01596 15.66
    278.15  0.02936 15.67
    278.15  0.02945 15.72
    278.15  0.03019 15.75
    278.15  0.03582 15.67
    
    283.15  0.01483 15.62
    283.15  0.01644 15.58
    283.15  0.01834 15.55
    283.15  0.02161 15.64
    283.15  0.03689 15.59
    
    288.15  0.02160 15.41
    288.15  0.02583 15.48
    288.15  0.02856 15.49
    288.15  0.06849 15.55
    
    298.15  0.02231 15.36
    298.15  0.02533 15.33
    298.15  0.02877 15.28
    298.15  0.03089 15.34
    298.15  0.03598 15.39
    298.15  0.03872 15.29
    298.15  0.04804 15.32
    298.15  0.04944 15.29
    298.15  0.05257 15.31
    298.15  0.05270 15.34

    308.15  0.02048 15.33
    308.15  0.02197 15.26
    308.15  0.04933 15.35
    308.15  0.05279 15.28
    
    318.15  0.02435 15.36
    318.15  0.02842 15.44
    318.15  0.05136 15.50
    318.15  0.08118 15.40
    
    328.15  0.01547 15.60
    328.15  0.02356 15.59
    328.15  0.05128 15.53
    328.15  0.05295 15.50
    
    338.15  0.03120 15.68
    338.15  0.03484 15.62
    338.15  0.03717 15.64
    338.15  0.07253 15.70
    ];

% Egan, E.P., Jr., Luff, B.B. Heat of solution, heat capacity and density
% of aqueous urea solutions at 25C // J. Chem. Eng. Data. 1966. 11(2).
% 192-194. DOI: 10.1021/je60029a020
Egan1966_dat = [ % for 25C: m, dH(cal/mol)
    0.319   3633.82
    0.643   3604.48
    0.982   3577.56
    1.329   3552.56
    1.709   3526.44
    2.110   3500.73
    2.426   3481.23
    2.791   3459.59
    3.175   3438.17
    3.567   3418.34
    3.985   3396.62
    4.366   3377.30
    4.761   3359.21
    5.163   3341.92
    5.575   3324.01
    6.001   3307.04
    6.438   3289.84
    6.867   3274.04
    7.307   3258.23
    7.806   3241.15
    8.290   3225.45
    8.763   3210.71
    9.190   3198.20
    9.660   3184.72
    10.097  3172.57
    10.576  3160.25
    11.106  3146.09
    11.573  3134.54
    12.061  3122.85
    12.573  3110.96
    13.093  3098.47
    13.600  3088.02
    14.218  3075.70
    14.760  3065.08
    15.302  3054.29
    15.907  3043.52
    16.512  3032.52
    17.132  3022.36
    17.659  3013.90
    18.309  3004.02
    ];


% UTF8 Encoding
% Прусов А.Н., Захаров А.Г., Крестов Г.А. Термохимические исследования 
% \epsilon - аминокапроновой кислоты, мочевины и моногидрата глюкозы при 
% 25-55C // Проблемы сольватации и комплексообразования. Межвузовский
% сборник. Иваново, 1979.
Prusov1979_dat = [ % m(UR), dH(25C) dH(35C) dH(45C) dH(55C) (cal/mol)
%    eps     3680    3700    3723    3747 % Transferred to dHsol_inf
    0.005   3673    3692    3715    3741
    0.01    3670    3689    3712    3737
    0.02    3665    3684    3707    3733
    0.03    3662    3681    3704    3730
    0.04    3659    3678    3701    3727
    0.05    3656    3675    3698    3724
    0.10    3646    3665    3688    3715
    0.20    3632    3651    3673    3701
    0.30    3621    3639    3662    3691
    0.40    3612    3630    3653    3682
    0.50    3604    3622    3645    3674
    0.60    3596    3614    3637    3667
    0.70    3590    3607    3631    3660
    0.80    3583    3601    3624    3654
    0.90    3577    3595    3618    3648
    1.00    3572    3589    3613    3643
    1.10    3567    3584    3607    3638
    1.20    3562    3579    3602    3639
    1.30    3557    3574    3597    3626
    1.40    3552    3569    3593    3624
    1.50    3547    3564    3588    3620
    1.60    3543    3560    3584    3615
    ];

% Gatta, G.D., Badea, E., J\'{o}\'{z}wiak, M., Vecchio, P.D. Thermodynamics
% of solvation of urea and some monosubstituted N-alkylureas in water
% at 298.15 K // J. Chem. Eng. Data. 2007. 52(2). 419-425.
% DOI: 10.1021/je060360n
Gatta2007_dat = [ % TK, m*10-2, dHsol (kJ/mol)
    296.84  0.601   15.38
    296.84  1.590   15.02
    296.84  2.171   15.44
    296.84  2.858   15.30
    296.84  5.032   15.29
    
    306.89  0.695   15.10
    306.89  1.162   15.28
    306.89  1.367   15.14
    306.89  1.461   15.10
    306.89  2.233   15.18
    
    316.95  0.628   14.96
    316.95  0.738   14.65
    316.95  0.822   15.15
    316.95  1.068   15.40
    316.95  1.289   15.30
    316.95  1.664   15.21
    316.95  1.782   14.88
    316.95  2.540   14.86
    ];

% Pack all data into structures and convert to required units
s = struct('dH',[], 'x',[], 'T',[], 'marker', []);
dat = struct('Ega66', s, 'Sub69', s, 'Sub71', s, 'Kus10', s, 'Pru79', s, 'Gat07', s);

dat.Ega66.x = UW_mtox(Egan1966_dat(:,1));
dat.Ega66.dH = Egan1966_dat(:,2) * 4.184; % CAL to J
dat.Ega66.T = 298.15 * ones(size(dat.Ega66.x));
dat.Ega66.marker = 'bx';

dat.Sub69.T = Subramanian1969_dat(:,1) + 273.15;
dat.Sub69.x = UW_mtox(Subramanian1969_dat(:,2) * 1e-3); %mmol/kg to mol.frac.
dat.Sub69.dH = Subramanian1969_dat(:,3) * 4.184; % CAL to J
dat.Sub69.marker = 'bd';

dat.Sub71.T = Subramanian1971_dat(:,1) + 273.15;
dat.Sub71.x = UW_mtox(Subramanian1971_dat(:,2) * 1e-3); %mmol/kg to mol.frac
dat.Sub71.dH = Subramanian1971_dat(:,3) * 4.184; % CAL to J
dat.Sub71.marker = 'bs';

dat.Kus10.T = Kustov2010_dat(:,1);
dat.Kus10.x = UW_mtox(Kustov2010_dat(:,2));
dat.Kus10.dH = Kustov2010_dat(:,3) * 1000; % kJ TO J
dat.Kus10.marker = 'm^';

x = UW_mtox(Prusov1979_dat(:,1));
dat.Pru79.x = repmat(x, 4, 1);
dat.Pru79.T = [25*ones(size(x)); 35*ones(size(x)); 45*ones(size(x)); 55*ones(size(x))] + 273.15;
dH = Prusov1979_dat(:,2:5) * 4.184; % CAL to J
dat.Pru79.dH = dH(:);
dat.Pru79.marker = 'k+';

dat.Gat07.T = Gatta2007_dat(:,1);
dat.Gat07.x = UW_mtox(Gatta2007_dat(:,2) * 1e-2); %mol/kg*10-2 to mol.frac.
dat.Gat07.dH = Gatta2007_dat(:,3) * 1000; % kJ TO J
dat.Gat07.marker = 'b<';

dmixH = @(g00, x, T) td_func(g00, x, T, 'dmixH');
dHdil_func = @(g00, x1, x2, T) dmixH(g00, x2, T) ./ x2 - dmixH(g00, x1, T) ./ x1;
dHsol_func = @(g00, ser) URprop.dmH_func(ser.T) + dHdil_func(g00, 1, ser.x, ser.T);
ser = DataGroup('dHsol', {'dH', 'x', 'T', 'marker'}, dHsol_func, 'rel');
ser = ser.addSeries('Ega66', dat.Ega66);
ser = ser.addSeries('Sub69', dat.Sub69);
ser = ser.addSeries('Sub71', dat.Sub71);
ser = ser.addSeries('Pru79', dat.Pru79);
ser = ser.addSeries('Gat07', dat.Gat07);
ser = ser.addSeries('Kus10', dat.Kus10);
ser = ser.finalize;
ser = ser.cfgFigAxes('$T$, K', @(ser) (ser.T), ...
    '$\left( \Delta{H}_\mathrm{sol}^\mathrm{exp} - \Delta{H}_\mathrm{sol}^\mathrm{calc} \right) / \Delta{H}_\mathrm{sol}^\mathrm{exp} \cdot 100$', ...
    @(ser, res) (res * 100));
end

function ser = load_Cp_data
global H2Oprop URprop
Mw = H2Oprop.M; % M(H2O), g/mol
Mur = URprop.M; % M(UREA), g/mol

s_to_Cp_func = @(Cp, x) ((1 - x)*Mw + x*Mur) .* Cp;

% Gucker, F.T., Ayres, F.D. The specific heat of aqueous solution of
% urea from 2 to 40C an the apparent molal heat capacity of urea //
% // J. Am. Chem. Soc., 1937, 59 (11), pp 2152–2155
% DOI: 10.1021/ja01290a020
Gucker1937_dat = [% m   s(Specific heat, cal/(C*g), relative to H2O)
    % m         s(2C)       s(5C)       s(10C)      s(20C)      s(30C)      s(40C)
    0.09353     0.99543     0.99569     0.99590     0.99630     0.99654     0.99676
    0.16398     0.99195     0.99236     0.99281     0.99353     NaN         NaN
    0.30232     0.98551     0.98619     0.98706     0.98815     0.98887     0.98943
    0.50556     0.97645     0.97747     0.97871     0.98046     0.98162     0.98247
    0.76971     0.96532     0.96653     0.96848     0.97097     0.97254     0.97368
    1.0428      0.95444     0.95613     0.95849     0.96160     0.96362     NaN
    1.9689      0.92214     0.92460     0.92803     0.93285     0.93616     NaN
    2.2842      0.91262     0.91528     0.91894     0.92414     0.92774     0.93048
    3.4826      0.88016     0.88334     0.88766     0.89404     0.89849     NaN
    4.7930      0.85108     0.85451     0.85917     0.86622     0.87135     0.87536
    6.4168      0.82147     0.82498     0.82994     0.83742     0.84292     NaN
    8.0736      0.79682     0.80037     0.80537     0.81311     0.81895     NaN
    10.5323     NaN         0.77062     0.77562     0.78348     0.78960     0.79467
    13.5186     NaN         NaN         NaN         0.75531     0.76149     NaN
    17.5767     NaN         NaN         NaN         NaN         0.73255     0.73817
    ];

Egan1966_dat = [ %m s(Specific heat, cal/(C*g), cal = 4.184J)
    0       0.9980
    0.319   0.9890
    0.643   0.9749
    0.982   0.9634
    1.329   0.9518
    1.709   0.9391
    2.110   0.9300
    2.426   0.9223
    2.791   0.9118
    3.175   0.9045
    3.567   0.8954
    3.985   0.8848
    4.366   0.8753
    4.761   0.8673
    5.163   0.8620
    5.575   0.8509
    6.001   0.8469
    6.438   0.8371
    6.867   0.8324
    7.307   0.8260
    7.806   0.8171
    8.290   0.8114
    8.763   0.8052
    9.190   0.8025
    9.660   0.7947
    10.097  0.7893
    10.576  0.7841
    11.106  0.7782
    11.573  0.7747
    12.061  0.7710
    12.573  0.7658
    13.093  0.7604
    13.600  0.7583
    14.218  0.7514
    14.760  0.7481
    15.302  0.7440
    15.907  0.7387
    16.512  0.7343
    17.132  0.7328
    17.659  0.7264
    18.309  0.7222
    ];

% Picker P., Leduc P.A., Philip P.R., Desnoyers J.E. Heat capacity of
% solutions by flow microcalorimetry // J. Chem. Thermodynamics. 1971.
% 3. 631-642.
Picker1971_dat = [ %m, Cp_phi(J/mol/K) (297.3K)
    0.09775 86.2
    0.35243 89.4
    0.65447 91.5
    1.0052  93.1
    1.5018  95.8
    1.8171  97.1
    2.1352  98.5
    3.0196  101.2
    3.7333  103.6
    4.7296  105.8
    5.5073  108.1
    7.4693  109.6
    8.9687  111.9
];

% White C.M. A study of the heat capacity of aqueous solution of urea
% and mannite // JACS, 1936, 58(9), 1620-1623.
% DOI: 10.1021/ja01300a034
White1936_dat = [ %m, s(cal15/deg/g), cal15 = 4.1855
    0.0100  0.99745
    0.0100  0.99735
    
    0.0300  0.99691
    0.0300  0.99688
    
    0.0500  0.99604
    0.0500  0.99609
    
    0.1000  0.99420
    0.1000  0.99420
    
    0.1250  0.99296
    0.1250  0.99301
    
    0.1522  0.99214
    0.1522  0.99221
    
    0.1998  0.99029
    0.1998  0.99037
    
    0.2997  0.98669
    0.2997  0.98654
    
    0.5000  0.97923
    0.4995  0.97933
    
    0.6993  0.97250
    0.6993  0.97248
    
    0.9990  0.96248
    0.9990  0.96245
    ];

% Hakin, A.W. Beswick C.L., Duke M.M. Thermochemical and volumetric
% properties of aqueous urea systems. Heat capacities and volumes of
% transfer from water to urea-water mixtures for some 1:1 electrolytes at
% 298.15 K // J. Chem. Soc., Faraday Trans.,1996, 92(2), 207-214.
Hakin1996_dat = [ % T, m, Cp2phi (ARTICLE ALSO CONTAINS DENSITIES)
    288.15  0.8809  80.9
    288.15  1.8310  85.2
    288.15  2.8536  88.8
    288.15  3.9596  92.1
    288.15  5.1913  95.0
    288.15  6.9168  98.5
    288.15  7.7112  99.7
    288.15  10.8404 103.7
    288.15  16.0781 108.0
    298.15  0.8715  91.0
    298.15  1.8189  94.1
    298.15  2.8850  96.8
    298.15  3.8384  99.1
    298.15  4.9558  100.8
    298.15  7.0396  104.2
    298.15  9.1724  106.4
    298.15  11.0028 108.0
    298.15  15.2352 111.0

    308.15  0.8809  99.7
    308.15  1.8310  101.8
    308.15  2.8536  103.6
    308.15  3.9596  105.4
    308.15  5.1913  107.0
    308.15  6.9168  109.2
    308.15  7.7112  110.0
    308.15  10.8404 112.4
    308.15  16.0781 115.3
    
    313.15  0.8809  103.0
    313.15  1.8310  105.0
    313.15  3.9596  106.5
    313.15  5.1913  108.0
    313.15  6.9168  109.7
    313.15  7.7112  111.6
    313.15  10.8404 114.5
    313.15  16.0781 116.7
    ];

% Causi S., De Lisi R., Milioto S., Tirone N. Dodecyltrimethylammonium
% bromide in water-urea mixtures. Volumes, heat capacities and
% conductivities // J. Phys. Chem. 1991. 95. 5664-5673.
Causi1991_dat = [ % m, Cpphi
    0.09307 91.5
    0.2010  90.0
    0.3000  94.7
    0.7392  91.7
    1.0002  95.3
    1.9598  95.2
    2.9941  96.6
    4.1277  102.0
    4.9902  101.0
    5.9557  103.5
    6.9995  104.3
    ];


s = struct('Cp',[], 'x',[], 'T',[], 'marker', []);
dat = struct('Guc37', s, 'Ega66', s, 'Pic71', s, 'Whi36', s, 'Hak96', s, 'Cau91', s);

T_ser = [2 5 10 20 30 40] + 273.15;
Cp_w =  [4.213 4.205 4.195 4.185 4.180 4.179]; % Cp(H2O) J/(mol*K)
Guc37_x = UW_mtox(Gucker1937_dat(:,1));
for i = 1:numel(T_ser)
   dat.Guc37.x = [dat.Guc37.x; Guc37_x];
   dat.Guc37.T = [dat.Guc37.T; T_ser(i)*ones(size(Guc37_x))];
   dat.Guc37.Cp = [dat.Guc37.Cp; s_to_Cp_func(Gucker1937_dat(:,i+1) .* Cp_w(i), Guc37_x)];   
end

ii = ~isnan(dat.Guc37.Cp);
dat.Guc37.Cp = dat.Guc37.Cp(ii);
dat.Guc37.x = dat.Guc37.x(ii);
dat.Guc37.T = dat.Guc37.T(ii);
dat.Guc37.marker = 'kx';

dat.Ega66.x = UW_mtox(Egan1966_dat(:,1));
dat.Ega66.Cp = s_to_Cp_func(Egan1966_dat(:,2) * 4.184, dat.Ega66.x);
dat.Ega66.T = 298.15 * ones(size(dat.Ega66.x));
dat.Ega66.marker = 'bs';

m = Picker1971_dat(:,1);
dat.Pic71.x = UW_mtox(m);
Cpphi = Picker1971_dat(:,2);
dat.Pic71.Cp = (Cpphi + 4.1796./(m*1e-3)) ./ (Mur + 1./(m*1e-3));
dat.Pic71.Cp = s_to_Cp_func(dat.Pic71.Cp, dat.Pic71.x);
dat.Pic71.T = 297.3 * ones(size(dat.Pic71.x));
dat.Pic71.marker = 'bv';

dat.Whi36.x = UW_mtox(White1936_dat(:,1));
dat.Whi36.T = 298.15 * ones(size(dat.Whi36.x));
dat.Whi36.Cp = s_to_Cp_func(White1936_dat(:,2) * 4.1855, dat.Whi36.x);
dat.Whi36.marker = 'bo';

dat.Hak96.T = Hakin1996_dat(:,1);
m = Hakin1996_dat(:,2);
dat.Hak96.x = UW_mtox(m);
Cpphi = Hakin1996_dat(:,3);
Cpw = Cp_H2O_L(dat.Hak96.T) ./ Mw;
dat.Hak96.Cp = (Cpphi + Cpw./(m*1e-3)) ./ (Mur + 1./(m*1e-3));
dat.Hak96.Cp = s_to_Cp_func(dat.Hak96.Cp, dat.Hak96.x);
dat.Hak96.w = 0.5*ones(size(dat.Hak96.T));
dat.Hak96.marker = 'm<';

m = Causi1991_dat(:,1);
dat.Cau91.x = UW_mtox(m);
Cpphi = Causi1991_dat(:,2);
dat.Cau91.Cp = (Cpphi + 4.1792./(m*1e-3)) ./ (Mur + 1./(m*1e-3));
dat.Cau91.Cp = s_to_Cp_func(dat.Cau91.Cp, dat.Cau91.x);
dat.Cau91.T = 298 * ones(size(dat.Cau91.x));
dat.Cau91.marker = 'b^';


Cp_func = @(g00, ser) (1 - ser.x) .* Cp_H2O_L(ser.T) + ser.x.*Cp_UR_L(ser.T) + td_func(g00, ser.x, ser.T, 'dmixCp');
ser = DataGroup('Cp', {'Cp', 'x', 'T', 'marker'}, Cp_func , 'rel');
ser = ser.addSeries('Whi36', dat.Whi36);
ser = ser.addSeries('Guc37', dat.Guc37);
ser = ser.addSeries('Ega66', dat.Ega66);
ser = ser.addSeries('Pic71', dat.Pic71);
ser = ser.addSeries('Cau91', dat.Cau91);
ser = ser.addSeries('Hak96', dat.Hak96);
ser = ser.finalize();
ser = ser.cfgFigAxes('$x\left(\mathrm{(NH_2)_2CO}\right)$', @(ser) ser.x,...
    '$\left(C_p^\mathrm{exp} - C_p^\mathrm{calc}\right) / C_p^\mathrm{exp} \cdot 100$', @(ser, res) res * 100);
end