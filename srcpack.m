% SRCPACK  Packs the sources into one self-extracting M-file. It is
% useful to make one-file supplementary material from the program that
% contains a lot of small files.
%
% Usage:
%   srcpack(outfile, filemasks, filedirs)
%
% Inputs (all are optional)
%   outfile   -- string with output file name. Default value:
%                'packed/source.m'
%   filemasks -- cell arrays with wildcards (e.g. '*.m'). Default value:
%                {'*.m', '*.rb', '*.yaml', '*.txt', 'COPYING', 'README',
%                 'output/dirinfo}
%   filedirs  -- cell arrays with directory names. Default value:
%                {'.', './water_eps'}

% (C) 2016 Alexey Voskov
% License: MIT
function srcpack(outfile, filemasks, filedirs)
if nargin == 0
    outfile = 'packed/source.m';
    filemasks = {'*.m', '*.rb', '*.yaml', '*.txt', 'COPYING', 'README', 'dirinfo'};
    filedirs = {'.', './water_prop', './output'};
end
if ~iscell(filemasks)
    filemasks = {filemasks};
end
% Read stub
stub = {};
fid = fopen('srcpack.m');
while 1
    tline = fgetl(fid);
    if ~ischar(tline); break; end
    stub = [stub; tline];
end
fclose(fid);
i1 = find(strcmp(stub, '%<')); i2 = find(strcmp(stub, '%>'));
stub = stub((i1+1):(i2-1));
for i = 1:numel(stub); x = stub{i}; stub{i} = [x(3:end), sprintf('\n')]; end;
stub = [stub{:}];
% Get the list of files
files = {};
for i = 1:numel(filedirs)
    for j = 1:numel(filemasks)
        dirout = dir([filedirs{i}, '/', filemasks{j}]);
        for k = 1:numel(dirout)
            files = [files; [filedirs{i}, '/', dirout(k).name]];
        end
    end
end
% Begin packing
lines = {stub; '%= BEGINNING OF THE SOURCE CODE'};
for i = 1:numel(files)
    filename = files{i};
    fid = fopen(filename);
    lines = [lines; ['%>', filename]];
    while 1
        tline = fgetl(fid);
        if ~ischar(tline)
            break
        end
        lines = [lines; ['% ', tline]];
    end    
    fclose(fid);    
end
% Brief report about loaded files
fprintf('Total number of lines: %d\n', numel(lines));
for i = 1:numel(files)
    fprintf('  %s\n', files{i});
end
% Save source code
fid = fopen(outfile, 'w');
for i = 1:numel(lines)
    fprintf(fid, '%s\n', lines{i});
end
fclose(fid);
end
%<
% % This is self-extracting archive with source code. Run it inside empty
% % folder (directory) to extract the sources.
% % Unpacker author: (C) 2016 Alexey Voskov
% % Unpacker license: MIT (X11) license
% function source
% fid = fopen('source.m');
% % Find the beginning of the source code
% tline = '%%%%%';
% while numel(tline) < 2 || ~strcmp(tline(1:2), '%=')
%     tline = fgetl(fid);
%     if ~ischar(tline); error('The file is corrupted'); end
% end
% % Unpack all files
% tline = fgetl(fid);
% if numel(tline) < 2 || ~strcmp(tline(1:2), '%>')
%     error('The file is corrupted');
% end
% % Write all files to the disk
% while ischar(tline);
%     filename = tline(3:end);    
%     fprintf('Unpacking %s\n', filename);    
%     fid_out = fopen_ex(filename, 'w');
%     while 1
%         tline = fgetl(fid);
%         if ~ischar(tline) || (numel(tline) > 2 && strcmp(tline(1:2), '%>'))
%             break
%         end
%         if numel(tline) >= 2; fprintf(fid_out, '%s\n', tline(3:end)); end
%     end
%     fclose(fid_out);
% end
% end
% function fid = fopen_ex(filename, mode)
% ii = find(filename == '/');
% if ~isempty(ii)
%     dirname = filename(1:(ii(end)-1));
%     if ~strcmp(dirname, '.') && ~exist(dirname, 'dir'); mkdir(dirname); end
% end
% fid = fopen(filename, mode);
% end
%>