% TD_MODEL_FIT  Fits the thermodynamic model using lsqnonlin
% function. After fitting it tries to round parameters and left
% as few significant numbers as possible (it controls relative
% error of sigma2 to be not higher than 0.1%)
%
% Usage:
%   [a, sa, sigma2] = td_model_fit(a0, minfunc)
% Inputs:
%   a -- initial approximation (will be transferred to lsqnonlin)
%   minfunc -- @(a) (...) anonymous function handle (will be 
%              transferred to lsqnonlin
% Outputs:
%   a -- optimized parameters
%   sa -- 95% confidence intervals (t-distribution and Jacobian
%         are used for its estimation)
%   sigma2 -- square of model standard error
%
% See also: display_parameters, lsqnonlin

% Programming: Alexey Voskov
% License: MIT
function [a, sa, sigma2] = td_model_fit(a0, minfunc)
opt = optimset('Display', 'iter');
[a, ~, res, ~, ~, ~, J] = lsqnonlin(minfunc, a0, [], [], opt);
% Calculate confidence intervals
f = numel(res) - numel(a);
sigma2 = (res'*res) ./ f; t = tinv(0.975, f);
c = full(sigma2*inv(J'*J));
sa = sqrt(diag(c));
% Type the results (regression properties)
extra_digits = 0;
sigma2_rounded = sigma2 * 100;
while abs(sqrt(sigma2_rounded) - sqrt(sigma2)) / sqrt(sigma2) >= 0.0001
    [~,ndigits,a_rounded] = display_parameters(a,res,J,extra_digits);
    res_rounded = minfunc(a_rounded);
    sigma2_rounded = (res_rounded'*res_rounded) ./ f;
    extra_digits = extra_digits + 1;
end
a = a_rounded;
display_parameters(a_rounded, res_rounded, J, extra_digits);
end
