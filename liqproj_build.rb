#TERNAPI
#
# TernAPI-C script for construction of the liquidus projection
# of the H2O(1)-UREA(2)-NH4SO3NH2(3) system
#
# (C) 2016 Alexey Voskov
# License: MIT

class SVGGibbsTriangle
	# Draws an arrow on the diagram
	# x2, x3 -- coordinates of the arrow center (vertex)
	# angle -- angle of the direction in degrees
	#          0 -- up, 90 -- right, 180 -- bottom, 270 -- left
	# opts -- arrow properties (Hash):
	#         arrow-width  arrow size in pixels (15 is default)
	#         stroke       line style/color (default is darkblue)
	#         stroke-width line width (default is 1)
	def drawarrow(x2, x3, angle, opts)
		opt_default = {"arrow-width"=>"15", "stroke"=>"darkblue",
			"stroke-width"=>"1"}
		opts = options_extend(opts, opt_default)		
		angle += 135.0
		pi = Math::PI
		sinx = Math::sin(angle / 360.0 * (2.0 * pi))
		cosx = Math::cos(angle / 360.0 * (2.0 * pi))
		arrow_width = opts["arrow-width"].to_i;
		xy0 = self.x23_to_rxy(x2, x3, true)
		x0, y0 = xy0["x"], xy0["y"]
		x = [x0 + arrow_width*sinx, x0, x0 + arrow_width*cosx];
		y = [y0 - arrow_width*cosx, y0, y0 + arrow_width*sinx];
		#opts = {"fill"=>"none", "stroke"=>"darkblue", "stroke-width"=>"1"}
		self.polyline(x, y, opts)
	end
end


# Load YAML file with our thermodynamic model
diag = nil
File.open('model_pdh.yaml') do |fp| 
	yaml = YAML.load(fp)
	diag = PhaseDiagram.new(yaml)
	diag.dnum = 1000;
end
# Initialize the set of temperatures
tv = []
(250..400).step(10) { |t| tv << t }
# Initialize variables for results
l_polyx = []
monolines = [{}, {}, {}]
# Add ternary eutectic
# WARNING! MANUALLY SET COORDINATES!
terneut_x23 = [0.1312, 0.1130];
monolines[0][244.3] = terneut_x23
monolines[1][244.3] = terneut_x23
monolines[2][244.3] = terneut_x23

# Calculate isothermal sections
tv.each do |t|
	puts "Calculating section at T=#{t} K"
	diag.set_parameter('t', t)
	diag.calculate
	# Find L region and get its external boundaries in not indexed ((x2,x3) coordinates)
	reg_L = diag.regions.select {|r| r.regname == 'L' }
	#raise "Invalid number of L regions (#{reg_L.size}) at T=(#{t} K)" if reg_L.size != 1
	polyx = reg_L[0].extbound_polygon_xcoords(smooth: true)
	raise "Multiple boundaries of L regions" if polyx.size != 1
	l_polyx << polyx[0]
	# Find triangles
	regs_3ph = diag.regions.select {|r| r.phnum == 3}
	regs_3ph.each do |reg|
		xcoords = reg.trilist_xcoords		
		x2coords = [xcoords[0][0][0][0], xcoords[1][0][0][0], xcoords[2][0][0][0]] # VERTEX,COMPONENT,TRIANGLE
		x3coords = [xcoords[0][1][0][0], xcoords[1][1][0][0], xcoords[2][1][0][0]]
		tripoints = [x2coords, x3coords].transpose
		# Find the triangle phase composition
		component_present = [false, false, false]
		monoline_point = nil
		tripoints.each do |point|
			if point[0] == 0.0 && point[1] == 0.0
				component_present[0] = true 
			elsif point[0] == 1.0 && point[1] == 0.0
				component_present[1] = true
			elsif point[0] == 0.0 && point[1] == 1.0
				component_present[2] = true
			else
				monoline_point = point
			end
		end

		# Add point to the monovariant line
		ind = component_present.find_index(false)
		monolines[ind][t] = monoline_point
	end
end

# Add binary eutectic to the ends of the monovariant lines
# WARNING! MANUALLY SET COORDINATES!
eut23_x23 = [0.6262, 0.3738] # UR-ASM eutectic point
eut13_x23 = [0.0, 0.1184]    # H2O-ASM eutectic point
eut12_x23 = [0.1255, 0.0]    # H2O-UR eutectic point
monolines[0][338.5] = eut23_x23 # UR-ASM
monolines[1][254.9] = eut13_x23 # H2O-ASM
monolines[2][261.8] = eut12_x23 # H2O-UR eutectic point

# Initialize Gibbs-Roozeboom triangle
opts = {
	'boxsize' => 400,
	'phbound_width' => 2,
	'tieline_width' => 1,
	'gridlines' => false,
	'ticklabels' => false,
	'xstep' => 0.2,
	'sign_regions' => false,
	'compnames' => ['H_2O', '(NH_2)_2CO', 'NH_4SO_3NH_2'],
	'components_M' => [18.02, 60.06, 114.12]
}
tri = SVGGibbsTriangle.new(opts)
tri.makegrid

# Draw liquidus projection isolines
l_polyx.each_index do |ind|
	polyx, t = l_polyx[ind], tv[ind]
	x2, x3 = polyx[0].to_ary.flatten, polyx[1].to_ary.flatten
	linewidth = (t == 300) ? tri.phbound_width : tri.tieline_width
	tri.drawpolygon(x2, x3, 'none', 'black', linewidth)
end

# Draw monovariant lines (thick blue solid lines)
monolines.each do |monoline|
	x23coords = monoline.values.transpose
	tri.drawpolygon(x23coords[0], x23coords[1], 'none', 'blue', tri.phbound_width)
end

# Show labels near eutectics
textopts = {'text-anchor' => 'middle'}
tri.drawtext(terneut_x23[0], terneut_x23[1] + 0.02, 'E', textopts)
tri.drawtext(eut12_x23[0] + 0.04, eut12_x23[1] - 0.04, 'e_{12}', textopts)
tri.drawtext(eut13_x23[0] - 0.04, eut13_x23[1], 'e_{13}', textopts)
tri.drawtext(eut23_x23[0] + 0.04, eut23_x23[1], 'e_{23}', textopts)

# Draw arrows on monolines
monolines.each do |ml|
	# Find the point to use as an arrow center
	ind = (ml.size / 2).round.to_i - 1
	key_prev = ml.keys[ind - 1]
	key = ml.keys[ind]
	key_next = ml.keys[ind + 1]
	x23_0 = ml[key]
	# Find points of the monoline that are nearest to the arrow center
	xy_prev = tri.x23_to_rxy(ml[key_prev][0], ml[key_prev][1], true)
	xy_next = tri.x23_to_rxy(ml[key_next][0], ml[key_next][1], true)
	# Calculate arrow angle using line slope
	dx = xy_next["x"] - xy_prev["x"]
	dy = -(xy_next["y"] - xy_prev["y"])
	angle = Math::atan(dx / dy.abs) / (2 * Math::PI) * 360.0
	angle = 180 - angle if dy <= 0
	# Draw an arrow
	tri.drawarrow(x23_0[0], x23_0[1], angle + 180.0,
		"stroke-width": 2, "arrow-width": 7)
end

# Create EPS (with fonts converted to curves), PDF and PNG (300 DPI) versions
File.open("fig12a_raw.eps", "w") {|fp| fp << tri.to_eps }
File.open("fig12a.svg", "w") {|fp| fp << tri.to_svg }
`eps2eps -dNOCACHE fig12a_raw.eps fig12a.eps`
`epstopdf fig12a.eps`
`gs -r300 -sDEVICE=png16m -dBATCH -dNOPAUSE -o fig12a.png fig12a.pdf`
