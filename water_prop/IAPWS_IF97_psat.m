% IAPWS_IF97_psat  Implementation of IAPWS-IF97 equation for vapour
%                  pressure of liquid water (see chapter 8.1 and
%                  Table 34 of the article
% Usage:
%   ps = IAPWS_IF97_psat(T); % Calc. mode
%   IAPWS_IF97(); % Self-testing mode
%
% Inputs:
%   T - 1xN or Nx1 double - Temperature, K
%
% Outputs:
%   ps - 1xN or Nx1       - Saturation pressure, MPa
%
% Temperature interval
%   273.15 <= T <= 647.096 (K)
%
% Reference:
%   http://www.iapws.org/relguide/IF97-Rev.pdf

% Programming: Alexey Voskov
% License: MIT
function ps = IAPWS_IF97_psat(T)
% Recursive self-testing procedure
% (to call it, run this function without input and output arguments)
if (nargin == 0 && nargout == 0)
    % Reference values
    Tref  = [300            500           600];
    psref = [0.353658941e-2 0.263889776e1 0.123443146e2];
    pscalc = IAPWS_IF97_psat(Tref);
    fprintf('max. ps deviation: %.4e\n', max(abs(psref-pscalc)));
    % Show diagrams
    T = 274:1:647;
    ps = IAPWS_IF97_psat(T);
    semilogy(1000./T, ps);
    title('IAPWS-IF97 p_{sat}(T) for water');
    xlabel('1000/T, K^{-1}');
    ylabel('p_{sat}, MPa');
    return;
end
% Check input arguments
if ~isvector(T) || any(T < 273.15 | T > 647.096)
    error('IAPWS_IF97_past:invalidTvalue', ...
        'Invalid value of T');
end
% Coefficients of equation
ni = [...
     0.11670521452767e4 % 1
    -0.72421316703206e6
    -0.17073846940092e2
     0.12020824702470e5
    -0.32325550322333e7 % 5
     0.14915108613530e2
    -0.48232657361591e4
     0.40511340542057e6
    -0.23855557567849
     0.65017534844798e3 % 10
    ];
T0 = 1; % K
p0 = 1; % MPa
% Calculate intermediate coefficients
x = T/T0 + ni(9)./( (T/T0) - ni(10) );
A =       x.^2 + ni(1)*x + ni(2);
B = ni(3)*x.^2 + ni(4)*x + ni(5);
C = ni(6)*x.^2 + ni(7)*x + ni(8);
% Calculate vapour pressure (ps) -- aka saturation pressure (MPa)
ps = p0*( 2*C./(-B + sqrt(B.^2 - 4*A.*C)) ).^4;
end