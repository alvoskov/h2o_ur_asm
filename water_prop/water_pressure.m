% Estimate dependecy for the water vapour pressure using IAPWS-IF-97 data
% The dependency is valid for T = 280 - 370 K

% Programming: Alexey Voskov
% License: MIT
function water_pressure
T = 280:2:370;
psat = IAPWS_IF97_psat(T) * 1000; % KPa

pfunc = @(b,T) exp(b(1) + b(2)./T + b(3).*log(T) + b(4).*T);
% a) logarithmic regression
opt = optimset('Display', 'iter');
beta = lsqnonlin(@(p) (log(psat) - log(pfunc(p, T))), [0 0 0 0], [], [], opt);
% b) ordinary regression
[beta,~,res,~,~,~,J] = lsqnonlin(@(p) (psat - pfunc(p, T))./psat, beta, [], [], opt);
res_abs = psat - pfunc(beta, T);
ln_res_abs = log(psat) - log(pfunc(beta, T));
f = numel(T) - numel(beta);
k = numel(beta);
fprintf('Standard deviation of ptabl - pcalc: %.3e\n', calc_sy(res_abs, k));
fprintf('Confidence interval of ptabl - pcalc: %.3e\n', calc_sy(res_abs, k) * tinv(0.975, f));
fprintf('Standard deviation of ln(ptabl) - ln(pcalc): %.3e\n', calc_sy(ln_res_abs, k));
fprintf('Confidence interval of ln(ptabl) - ln(pcalc): %.3e\n', calc_sy(ln_res_abs, k) * tinv(0.975, f));

[~,ndigits,beta_rounded] = display_parameters(beta, res, J, 4);
ndigits
beta = beta_rounded;
res = (psat - pfunc(beta_rounded, T))./psat;
display_parameters(beta, res, J, 0);
pfunc_precalc = @(T) exp(68.73133 - 7193.738./T - 7.896606.*log(T) + 5.17166e-3.*T);
plot(T, 100*(psat - pfunc(beta, T))./psat, 'bo', ...
    T, 100*(psat - pfunc_precalc(T))./psat, 'ro');
end

function sy = calc_sy(res, k)
sy = sqrt(sumsqr(res) ./ (numel(res) - k));
end
