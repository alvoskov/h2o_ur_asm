% Estimate dependecy for the water heat capacity using IAPWS-IF-97 data
% The dependency is valid for T = 274 - 371 K

% Programming: Alexey Voskov
% License: MIT
function water_Cp
T = (274:1:371)';
p = 0.101325;
[~,~,~,~,Cp] = IAPWS_IF97_liquid(p, T);

Cpfunc = @(b,T) b(1) + b(2).*T + b(3).*T.^2 + b(4)./T + b(5).*T.^(-0.7);
JJ = [ones(size(T)), T, T.^2, 1./T, T.^(-0.7)];
beta = JJ \ Cp;

res = Cp - Cpfunc(beta, T);
fprintf('sy: %.3e\n', sqrt(sumsqr(res) ./ (numel(res) - numel(beta))));

% b) ordinary regression
%opt = optimset('Display', 'iter');
%[beta,~,res,~,~,~,J] = lsqnonlin(@(b) (Cp - Cpfunc(b, T)), [0 0 0 0], [], [], opt);

[~,ndigits,beta_rounded] = display_parameters(beta, res, JJ, 5);
fprintf('-----------------');
res = Cp - Cpfunc(beta_rounded, T);
[~,ndigits,beta_rounded] = display_parameters(beta_rounded, res, JJ, 0);
%ndigits
%beta = beta_rounded;
plot(T, Cp, 'bo', T, Cpfunc(beta, T), 'k-');

end