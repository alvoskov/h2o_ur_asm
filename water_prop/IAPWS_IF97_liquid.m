% IAPWS_IF97_liquid  Implementation of IAPWS-IF97 equation 
%                    for liquid water
%
% Temperature and pressure intervals:
%   273.15<=T<=623.15 (K)
%   ps(T)<=p<=100 (MPa)
%
% Usage:
%   [v, h, u, s, cp] = IAPWS_IF97_liquid(p, T); % Calc. mode
%   IAPWS_IF97_liquid;                          % Self-testing mode
%
% Inputs:
%   p - 1xN or Nx1 double - pressure, MPa
%   T - 1xN or Nx1 double - temperature, K
%
% Reference:
%   http://www.iapws.org/relguide/IF97-Rev.pdf

% Programming: Alexey Voskov
% License: MIT
function [v, h, u, s, cp] = IAPWS_IF97_liquid(p, T)

% Recursive self-testing procedure
% (to call it, run this function without input and output arguments)
if (nargin == 0 && nargout == 0)
    fprintf('IAPWS_IF97_liquid self-test\n');
    % Test parameters
    Tref  = [300 300 500 273.16];
    pref  = [3   80  3   611.657e-6];
    Vref  = [0.100215168e-2 0.971180894e-3 0.120241800e-2 0.1000206297e-2]; % m3*kg^(-1)
    Href  = [0.115331273e3  0.184142828e3  0.975542239e3  0.611783e-3]; %kJ*kg^(-1)
    Uref  = [0.112324818e3  0.106448356e3  0.971934985e3  0]; % kJ*kg^(-1)
    Sref  = [0.392294792    0.368563852    0.258041912e1  0]; % kJ*kg^(-1)*K^(-1)
    cpref = [0.417301218e1  0.401008987e1  0.465580682e1  0.42198977111e+1]; % kJ*kg^(-1)*K^(-1)
    % Call function and compare the result
    [Vcalc, Hcalc, Ucalc, Scalc, cpcalc] = IAPWS_IF97_liquid(pref, Tref);
    print_maxdev(Vcalc, Vref, 'v');
    print_maxdev(Hcalc, Href, 'h');
    print_maxdev(Ucalc, Uref, 'u');
    print_maxdev(Scalc, Sref, 's');
    print_maxdev(cpcalc, cpref, 's');
    % Draw some graphics
    % 1) density
    T = 273.15:1:298.15; 
    p = 0.101325*ones(size(T));
    rho = 1./IAPWS_IF97_liquid(p, T);
    plot(T, rho);
    xlabel('T, K');
    ylabel('\rho, kg{\cdot}m^{-3}');    
    % Finish work
    return;
end

% Check input arguments
% 1) check T values
if ~isvector(T) || any(T < 273.15 | T > 623.15)
    error('IAPWS_IF97_past:invalidTvalue', ...
        'Invalid value of T');
end
% 2) check if p is not below psat
psat = IAPWS_IF97_psat(T);
if any((p + 1e-12) < psat)
    error('IAPWS_IF97_past:lowp', ...
        'Pressure is too low');
end

% Some constants
R = 0.461526; % kJ*kg(-1)*K(-1)
p0 = 16.53; % MPa
T0 = 1386;  % K

% Convert parameters to another units
pi_param = p / p0;
tau_param = T0./T;

% Parameters of IAPWS-IF97 model for liquid phase
    %Ii  Ji  ni
dat = [...
    0   -2   0.14632971213167 % 1
    0   -1  -0.84548187169114
    0   0   -0.37563603672040e1
    0   1    0.33855169168385e1
    0   2   -0.95791963387872 % 5
    0   3    0.15772038513228
    0   4   -0.16616417199501e-1
    0   5    0.81214629983568e-3
    1   -9   0.28319080123804e-3
    1   -7  -0.60706301565874e-3 % 10
    1   -1  -0.18990068218419e-1
    1   0   -0.32529748770505e-1
    1   1   -0.21841717175414e-1
    1   3   -0.52838357969930e-4
    2   -3  -0.47184321073267e-3 % 15
    2   0   -0.30001780793026e-3
    2   1    0.47661393906987e-4
    2   3   -0.44141845330846e-5
    2   17  -0.72694996297594e-15
    3   -4  -0.31679644845054e-4 % 20
    3   0   -0.28270797985312e-5
    3   6   -0.85205128120103e-9
    4   -5  -0.22425281908000e-5
    4   -2  -0.65171222895601e-6
    4   10  -0.14341729937924e-12 % 25
    5   -8  -0.40516996860117e-6
    8   -11 -0.12734301741641e-8
    8   -6  -0.17424871230634e-9
    21  -29 -0.68762131295531e-18
    23  -31  0.14478307828521e-19 % 30
    29  -38  0.26335781662795e-22
    30  -39 -0.11947622640071e-22
    31  -40  0.18228094581404e-23
    32  -41 -0.93537087292458e-25 % 34
    ];
Ii = dat(:,1);
Ji = dat(:,2);
ni = dat(:,3);

% IAPWS-IF97 polynomial formula
gamma     = zeros(size(T));
gamma_pi  = zeros(size(T));
gamma_tau = zeros(size(T)); 
gamma_tautau = zeros(size(T));
for i=1:34
    gamma = gamma + ...
       (ni(i)*(7.1-pi_param).^Ii(i).*(tau_param-1.222).^Ji(i) );
    gamma_pi = gamma_pi + ...
       (-ni(i)*Ii(i)*(7.1-pi_param).^(Ii(i)-1).*(tau_param-1.222).^Ji(i) );
    gamma_tau = gamma_tau + ...
       (ni(i)*(7.1-pi_param).^Ii(i).*Ji(i).*(tau_param-1.222).^(Ji(i)-1) );
    gamma_tautau = gamma_tautau + ...
       (ni(i)*(7.1-pi_param).^Ii(i).*Ji(i).*(Ji(i)-1).*(tau_param-1.222).^(Ji(i)-2) );   
end
% Calculate output values
v = gamma_pi.*pi_param.*(R*T) ./ (p*1e3);
h = (R*T).*(tau_param.*gamma_tau);
u = (R*T).*(tau_param.*gamma_tau - pi_param.*gamma_pi);
s = R*(tau_param.*gamma_tau - gamma);
cp = -R*tau_param.^2.*gamma_tautau;
end

function print_maxdev(Xcalc, Xref, Xname)
    delta = max(abs(Xcalc - Xref));
    Xref(Xref == 0) = nan;    
    reldev = max((abs(Xcalc - Xref))./Xref);
    fprintf('max. %s deviation: %.4e (%.2e%%)\n', ...
        Xname, delta, reldev);
end