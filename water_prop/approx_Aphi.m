% APPROX_APHI  Approximates Aphi coefficient for Debye-Huckel 
% contribution at 0.1MPa and T=263.15-433.15 K. It is used in
% Pitzer-Simonson-Clegg model.
%
% Data are taken from:
% [1] Archer D.G., Wang P. The dielectric constant of water and
% Debye-Huckel limiting law slopes // J. Phys. Chem. Ref. Data. 1990. V.19,
% N 2, 371-411.

% Programming: Alexey Voskov
% License: MIT
function approx_Aphi

dat = [ %T, K  Aphi(p=0.1MPa) Aphi (p=1.0MPa)
    263.15  0.37088 0.37073
    268.15  0.37368 0.37353
    273.15  0.37642 0.37627
    278.15  0.37920 0.37905
    283.15  0.38207 0.38191
    288.15  0.38506 0.38490
    293.15  0.38819 0.38803
    298.15  0.39148 0.39131
    303.15  0.39492 0.39474
    308.15  0.39852 0.39834
    313.15  0.40228 0.40209
    318.15  0.40620 0.40601
    323.15  0.41028 0.41008
    328.15  0.41452 0.41431
    333.15  0.41892 0.41870
    338.15  0.42347 0.42325
    343.15  0.42819 0.42796
    348.15  0.43307 0.43283
    353.15  0.43810 0.43785
    358.15  0.44330 0.44304
    363.15  0.44866 0.44839
    368.15  0.45419 0.45391
    373.15  0.45989 0.45959
    383.15  NaN     0.47147
    393.15  NaN     0.48406
    398.15  NaN     0.49063
    403.15  NaN     0.49740
    413.15  NaN     0.51154
    423.15  NaN     0.52654
    433.15  NaN     0.54248];
ii = ~isnan(dat(:,2));
Tv = dat(ii,1);
Aphi = dat(ii,2);
% Data series for extrapolation (emulation of superheated liquid water)
extr_eps = (dat(ii,2) - dat(ii,3))./dat(ii,3); extr_eps = extr_eps(end);
Tv_extr = dat(~ii,1);
Aphi_extr = dat(~ii,3) + extr_eps * dat(~ii,3);
Tv = [Tv; Tv_extr];
Aphi = [Aphi; Aphi_extr];
% Least squares method
opt = optimset('Display', 'iter');
a0 = [0 0 0 0];
[a,~,res,exitflag,~,~,J] = lsqnonlin(@(b) Aphi_func(b, Tv) - Aphi, a0, [], [], opt);
[da, ndigits, a_rounded] = display_parameters(a, res, J, 5)
display_parameters(a_rounded, res, J);
% Control of the approximation quality
Aphi_calc = Aphi_func(a, Tv);
sAphi = calc_sy(Aphi_calc - Aphi, numel(a0))
sAphi_rounded = calc_sy(Aphi_func(a_rounded, Tv) - Aphi, numel(a0))
sAphi_extr = calc_sy(Aphi_func(a_rounded, Tv_extr) - Aphi_extr, numel(a0))
% Control of the extracted expression
%sAphi_fix = calc_sy(-0.23746 - 5.539101e-3*Tv +5.351087e-6*Tv.^2 + 0.104524*Tv.^0.5 - Aphi, numel(a0))
sAphi_fix = calc_sy(-0.23746 - 5.5391e-3*Tv +5.3511e-6*Tv.^2 + 0.104524*Tv.^0.5 - Aphi, numel(a0))
% Show the result
Tvv = (-20:10:400) + 273.15;
plot(Tv, Aphi, 'bo', Tvv, Aphi_func(a, Tvv), 'k-');
% Show the result (text form)
fprintf('%s %s %s %s\n', 'T', 'A', 'A', 'dA');
Aphi_calc = Aphi_func(a_rounded, Tv);
for i = 1:numel(Tv)
    fprintf('%.2f %.5f %.5f %.5f\n', Tv(i), Aphi(i), Aphi_calc(i), Aphi(i) - Aphi_calc(i));
end
fprintf('%s %s %s %s\n', 'T', 'A', 'A', 'dA');
Aphi_calc = Aphi_func(a_rounded, Tv_extr);
for i = 1:numel(Tv_extr)
    fprintf('%.2f %.5f %.5f %.5f\n', Tv_extr(i), Aphi_extr(i), Aphi_calc(i), Aphi_extr(i) - Aphi_calc(i));
end
end

function sy = calc_sy(res, k)
    sy = sqrt(sumsqr(res) ./ (numel(res) - k));
end

function Aphi = Aphi_func(a, T)
Aphi = a(1) + a(2)*T + a(3)*T.^2 + a(4).*T.^0.5;
end
