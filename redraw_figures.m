% REDRAW_FIGURES  Redraws Fig.3,4,6,7-10 for the article
% by launching the scripts

% Programming: Alexey Voskov
% License: MIT
function redraw_figures
disp('===== FIGURE 3 =====');
tamman('ur_asm');

disp('===== FIGURE 4 ====');
ur_asm;

disp('===== FIGURES 6 AND 7 =====');
h2o_asm;

disp('===== FIGURES 8 AND 9 =====');
h2o_ur(true);

disp('===== FIGURE 11 + PART OF GRAPHICAL ABSTRACT =====');
ternary;
end
