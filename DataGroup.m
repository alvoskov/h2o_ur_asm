% DATAGROUP  Class for grouping thermodynamic data used in CALPHAD-type
% optimization. It supports arbitrary data format and thermodynamic
% models.
%
% See also DATAGROUP/DATAGROUP, DATAGROUP/ADDSERIES, DATAGROUP/CFGFIGAXES,
% DATAGROUP/FINALIZE, DATAGROUP/PLOTRELATIVEDEVIATIONS, DATAGROUP/RESIDUALS, 

% Programming: Alexey Voskov
% License: MIT
classdef DataGroup
    properties
        descr = '';
        fields = {};
        yfunc = [];
        series = struct();
        yfunc_inp = struct();
        resfunc = [];
        xlabel = ''; xcoordFunc = [];
        ylabel = ''; ycoordFunc = [];
        noLegend = false;
    end
    
    properties (Access=private)        
        finalized = false;
        wstyle = 'abs';
        markers = {'bo', 'bd', 'bs', 'bv', 'b^', 'b<', 'b>', 'b*', 'b+', 'b-', 'b.', 'bh', 'bp'};        
    end
    
    methods
        function obj = DataGroup(descr, fields, yfunc, wstyle)
        % DataGroup class constructor
        %
        % descr  -- text string with short description (tag)
        % fields -- cell array with fields names. Dependent variable field
        %           MUST BE THE FIRST
        % yfunc = @(param, ser) (....) function that calculates data series
        %         theoretical value (used to calculate delta)
        % wstyle -- style of data norming; text string:
        %           'abs' -- absolute deviations (no norming)
        %           'rel' -- relative deviations
        %           'mean' -- mean value for all data series will be used
        %                     as the norm
        
        % Check input data
        if ~ischar(descr)
            error('descr must be a string');
        end
        if ~iscell(fields)
            error('fields must be a cell array');
        end
        for i=1:numel(fields)
            if ~ischar(fields{i})
                error('All elements of fields must be strings');
            end
        end
        if ~isa(yfunc, 'function_handle')
            error('yfunc must be a function handle');
        end
        if ~ischar(wstyle)
            error('wstyle must be a string');
        end
        if all(~strcmp({'abs', 'rel', 'mean'}, wstyle))
            error('Unknown wstyle value %s', wstyle);
        end
        % Save initial settings
        obj.descr = descr;
        fields{end+1} = 'w';        
        obj.fields = fields;
        obj.yfunc = yfunc;
        obj.wstyle = wstyle;
        for i = 1:numel(obj.fields)
            f = obj.fields{i};
            obj.yfunc_inp.(f) = [];
        end
        end
        
        function obj = addSeries(obj, name, ser)
        % Adds user-defined data series 
        %   name -- name of the series
        %   ser -- struct that contains fields values for the series
        
        % Check input arguments
        if ~ischar(name)
            error('name must be a string');
        end
        if ~isstruct(ser)
            error('ser must be a struct');
        end
        if obj.finalized
            error('Data group is finalized');
        end
        % Controlled copying
        usrfields = fieldnames(ser);
        len = 1;
        for i = 1:numel(usrfields)
            f = usrfields{i};
            if any(strcmp(obj.fields, f))
                val = ser.(f);
                if isnumeric(val)
                    val = val(:)';
                    len = numel(val);
                end
                obj.series.(name).(f) = val;
            else
                error('Unknown field %s', usrfields{i});
            end
        end
        % Default statistical weights
        if ~isfield(obj.series.(name), 'w')
            obj.series.(name).w = ones(1, len);
        end            
        end
        
        function obj = finalize(obj)
        % Prepares data to the calculation of residuals vector
        % See also DataGroup/residuals
            
            % Concatenate data series
            sernames = fieldnames(obj.series);
            for sind = 1:numel(sernames)
                ser = obj.series.(sernames{sind});
                for fi = 1:numel(obj.fields)
                    fname = obj.fields{fi};
                    if isnumeric(ser.(fname))
                        obj.yfunc_inp.(fname) = [obj.yfunc_inp.(fname), ser.(fname)];
                    end
                end
            end
            % Generate weights and create residual function
            weights = [];
            yfield = obj.fields{1};            
            switch obj.wstyle
                case 'abs'
                    weights = 1;
                case 'rel'
                    weights = 1./obj.yfunc_inp.(yfield);
                case 'mean'
                    weights = 1./mean(abs(obj.yfunc_inp.(yfield)));
            end
            obj.resfunc = @(a) (obj.yfunc_inp.(yfield) - obj.yfunc(a, obj.yfunc_inp)).*obj.yfunc_inp.w.*weights;
            obj.yfunc_inp.resfunc = obj.resfunc;
            obj.yfunc_inp.resfunc_noUsrW = @(a) (obj.yfunc_inp.(yfield) - obj.yfunc(a, obj.yfunc_inp)).*weights;
            % Add residual functions for every data series
            sernames = fieldnames(obj.series);
            for sind = 1:numel(sernames)
                ser = obj.series.(sernames{sind});
                if strcmp(obj.wstyle, 'rel')
                    weights = 1./ser.(yfield);
                end
                obj.series.(sernames{sind}).resfunc = @(a) (ser.(yfield) - obj.yfunc(a, ser)).*ser.w.*weights;
                obj.series.(sernames{sind}).resfunc_noUsrW = @(a) (ser.(yfield) - obj.yfunc(a, ser)).*weights;
            end
            % Mark object as finalized
            obj.finalized = true;
        end
        
        function plotRelativeDeviations(obj, param, filename)
        % Draws the diagram with normed (by the way set by the user during
        % the call of DataGroup class constructor) deviations of all points
        % of all series. User-specified statistical weights are ignored.
            
            % Create figure and set axes labels
            figure;            
            xlabel(obj.xlabel, 'FontSize', 14, 'Interpreter', 'latex');
            ylabel(obj.ylabel, 'FontSize', 14, 'Interpreter', 'latex');            
            % Show series
            hold on;
            sernames = fieldnames(obj.series);
            yv = [];
            for i = 1:numel(sernames)
                ser = obj.series.(sernames{i});
                x = obj.xcoordFunc(ser);
                y = obj.ycoordFunc(ser, ser.resfunc_noUsrW(param));
                yv = [yv; y(:)];
                if isfield(ser, 'marker')
                    plot(x, y, ser.marker);
                else
                    plot(x, y, obj.markers{i});
                end
            end
            % Configure axes sizes
            frame = axis;
            ylim = max(abs(frame(3:4)));
            frame(3:4) = [-ylim ylim];
            axis(frame);
            
            w = obj.yfunc_inp.w;
            if w < 0.2
                wnorm = mean(w);
            else
                wnorm = 1;
            end
            
            res = obj.ycoordFunc(obj.yfunc_inp, obj.resfunc(param)) ./ wnorm;
            sigma = sqrt(sumsqr(res) ./ numel(res));
            plot(frame(1:2), [0 0], 'k-', ...
            frame(1:2), [-sigma, -sigma], 'k--', ...
            frame(1:2), [+sigma, +sigma], 'k--');
            hold off;
            box on;
            set(gcf, 'Units', 'Centimeters');
            set(gcf, 'Position', [0 0 12 10]);
            set(gcf, 'PaperPositionMode', 'auto');            
            % Configure legend
            set(gca, 'FontSize', 12);
            if ~obj.noLegend
                legend(sernames{:}, 'Location', 'Best');
            end
            % Save to file (creates PNG, EPS and PDF files)
            if nargin == 3
                print(sprintf('output/%s_raw.eps', filename), '-depsc');    
                print(sprintf('output/%s.png', filename), '-dpng', '-r300');
                dos(sprintf('eps2eps -dNOCACHE output/%s_raw.eps output/%s.eps', filename, filename));
                dos(sprintf('epstopdf output/%s.eps', filename));
            end
        end
        
        function res = residuals(obj, param)
        % Calculates residuals vector (with norms and user-defined
        % statistical weights)
        % See also DataGroup/finalize
            if ~obj.finalized
                error('Object is not finalized');
            end
            res = obj.resfunc(param);
        end
        
        function obj = cfgFigAxes(obj, xlabel, xfunc, ylabel, yfunc)
        % xfunc = @(ser) (...)
        % yfunc = @(ser, res) (...)            
            obj.xlabel = xlabel;
            obj.xcoordFunc = xfunc;
            obj.ylabel = ylabel;
            obj.ycoordFunc = yfunc;
        end
        
        function str = get.xlabel(obj)
            str = obj.xlabel;        
        end
                
        function obj = set.xlabel(obj, str)
            if ~ischar(str)
                error('xlabel must be a string');
            end
            obj.xlabel = str;
        end
        
        function fh = get.xcoordFunc(obj)
            fh = obj.xcoordFunc;
        end
        
        function obj = set.xcoordFunc(obj, fh)
            if ~isa(fh, 'function_handle')
                error('xcoordFunc must be a function handle');
            end
            obj.xcoordFunc = fh;
        end                        
        
        function str = get.ylabel(obj)
            str = obj.ylabel;        
        end
        
        function obj = set.ylabel(obj, str)
            if ~ischar(str)
                error('ylabel must be a string');
            end
            obj.ylabel = str;
        end        
        
        function obj = set.noLegend(obj, val)
            if ~isscalar(val) || (val ~= true && val ~= false)
                error('noLegend must be true or false');
            end
            obj.noLegend = val;
        end
        
        function val = get.noLegend(obj)
            val = obj.noLegend;
        end
        
        function fh = get.ycoordFunc(obj)
            fh = obj.ycoordFunc;
        end
        
        function obj = set.ycoordFunc(obj, fh)
            if ~isa(fh, 'function_handle')
                error('ycoordFunc must be a function handle');
            end
            obj.ycoordFunc = fh;
        end 
        
        function disp(obj, g00)
            fprintf('  DataGroup with properties:\n');
            fprintf('    Description (label): %s\n', obj.descr);
            fprintf('    Statistical weights style: %s\n', obj.wstyle);
            fprintf('    Series fields: ');
            for i = 1:numel(obj.fields)
                fprintf('%s ', obj.fields{i});
            end
            fprintf('\n');
            % Generate title for the table with data series
            fprintf('    Data series:\n');
            sernames = fieldnames(obj.series);
            fprintf('     %10s %4s %8s', 'Name', 'n', 'w');            
            if nargin == 2
                fprintf(' %8s %8s %8s', 'sumsqr', 'stdev', 'maxdev');
            end
            if any(strcmp(obj.fields, 'T'))
                fprintf(' %6s %6s', 'Tmin', 'Tmax');
            end
            if any(strcmp(obj.fields, 'x'))
                fprintf(' %7s %7s', 'xmin', 'xmax');
            end
            fprintf('\n');            
            % Show information about data series
            nsum = 0;
            for i = 1:numel(sernames)
                sername = sernames{i};
                ser = obj.series.(sername);                
                if nargin == 2
                    nsum = nsum + obj.disp_series(sername, ser, g00);
                else
                    nsum = nsum + obj.disp_series(sername, ser);
                end
            end
            % Show summary about the data
            if nargin == 2
                obj.disp_series('TOTAL', obj.yfunc_inp, g00);
            else
                obj.disp_series('TOTAL', obj.yfunc_inp);
            end
            fprintf('    Note: maxdev always ignores w, stdev uses w for series but ignores w for TOTAL\n');
            % Show information about axes
            fprintf('    Figure axis settings:\n');
            fprintf('      xlabel: %s\n', obj.xlabel);
            fprintf('      ylabel: %s\n', obj.ylabel);
            fprintf('\n');
        end
    end
    
    methods (Access=private)
        function n = disp_series(obj, sername, ser, g00)
            n = numel(ser.(obj.fields{1}));
            fprintf('     %10s %4d %8.2e', sername, n, mean(ser.w));
            if nargin == 4
                res = ser.resfunc(g00);
                resNoW = ser.resfunc_noUsrW(g00);                
                if strcmp(sername, 'TOTAL')
                    stdev = sqrt(sumsqr(res) ./ n);
                    maxdev = max(abs(resNoW));
                else
                    stdev = sqrt(sumsqr(resNoW) ./ n);
                    maxdev = max(abs(resNoW));
                end                    
                fprintf(' %8.2e %8.2e %8.2e', sumsqr(res), stdev, maxdev);
            end
            % Show minimal and maximal temperatures
            if isfield(ser, 'T')
                fprintf(' %6.2f %6.2f', min(ser.T), max(ser.T));
            end
            % Show minimal and maxima concentrations
            if isfield(ser, 'x')
                fprintf(' %7.5f %7.5f', min(ser.x), max(ser.x));
            end
            % Finalize the string
            fprintf('\n');            
        end
    end
end
