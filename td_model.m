% TD_MODEL Pitzer-Simonson thermodynamic model for the H2O-ASM and UR-ASM
% binary systems. 
%
% Usage:
%   [mu1, mu2, G, mu1_ex, mu2_ex, G_ex] = td_model(x, T, a, a_to_g)
% Inputs:
%   x -- molar fraction of ASM (as molecular species)
%   T -- temperature, K
%   a -- thermodynamic model parameters
%   a_to_g -- @(a,T) function handle (will convert a vector to
%             [W, U] vector used by Pitzer-Simonson model)
% Outputs:
%   mu1, mu2 -- chemical potentials
%   G -- Gibbs energy (per mol of MOLECULAR COMPONENTS)
%   mu2_ex, mu2_ex, G_ex -- excess thermodynamic functions
%
% Information about thermodynamic model
%   DmixG = Gconf + GPDH + GSR
%   x' = x./(1+x) -- molar fraction of NH4+ or NH4SO3-
%
%   Gconf/RT = (1+x)*RT*[(1-x')*log(1-x') + x'*log(x')]
%   GPDH/RT -(4*Ax.*Ix)./p.*log( (1 + p*sqrt(Ix)) ./ (1 + p*sqrt(0.5)) ) .* (1 + x);
%
% WARNING! All Gibbs energies are given per mol of NON-DISSOCIATED
% components (H2O/UR and ASM)
%
% References:
% [1] K.S. Pitzer, J.M. Simonson. Thermodynamics of multicomponent,
% miscible, ionic systems: theory and equations // J. Phys. Chem. 1986.
% 90. P.3005-3009.
%
% See also TEST_TD_MODEL, TD_MODEL_FIT, CALC_APHI

% Programming: Alexey Voskov
% License: MIT
function [mu1, mu2, G, mu1_ex, mu2_ex, G_ex] = td_model(x, T, a, a_to_g)
R = 8.3144598; % CODATA 2014
x(x <= eps) = eps;
% Mapping of input parameters vector to the model
gv = a_to_g(a, T);
W = gv(1); U = gv(2);
x_ = x./(1+x);
% Configurational contribution
mu1_conf = R*T.*(log(1-2*x_) );
mu2_conf = R*T.*(2*log(2*x_));
G_conf = R*T.*((1-2*x_).*log(1-2*x_) + 2*x_.*log(2*x_)).*(1+x);
% Short-range Gibbs energy contribution
G_sr = R*T .* (1 + x) .* ( 2*x_.*(1 - 2*x_).*(W + U*2*x_));
mu1_sr = R*T .* 4*x_.^2.*(W - U + 4*U*x_);
mu2_sr = R*T .* 2*(2*x_ - 1).^2.*(W + 4*U*x_);
% Pitzer-Debye-Huckel contribution
[g, lnf1, lnf2] = pdh_model(x, T);    
G_pdh = g .* R .* T;
mu1_pdh = lnf1 .* R .* T;
mu2_pdh = lnf2 .* R .* T;

% Sum up all contributions
mu1_ex = mu1_sr + mu1_pdh;
mu2_ex = mu2_sr + mu2_pdh;
G_ex = G_sr + G_pdh;

mu1 = mu1_conf + mu1_ex;
mu2 = mu2_conf + mu2_ex;
G = G_conf + G_ex;
end

function [g, mu1, mu2] = pdh_model(x, T)
Aphi = calc_Aphi(T);
Ax = Aphi * sqrt(1000 / 18.0153); % 2.918 at 298.15 K
p = 14.9;
Ix = x ./ (1 + x);

g = -(4*Ax.*Ix)./p.*log( (1 + p*sqrt(Ix)) ./ (1 + p*sqrt(0.5)) ) .* (1 + x);
x_ = Ix;
Ix0 = 0.5;
mu1 = -(2.*Ax.*x_.^(3/2) - 2.*Ax.*p.*x_.^2)./(x_.*p.^2 - 1);
mu2 = (4.*Ax.*x_.*log((p.*x_.^(1/2) + 1)./(Ix0.^(1/2).*p + 1)))./(p.*(x_ - 1)) - ...
    ((2*x_ - 1).*((4.*Ax.*log((p*x_.^(1/2) + 1)./(Ix0.^(1/2).*p + 1)))./p - (2.*Ax.*x_.*(x_ - 1))./(p.*x_ + x_.^(1/2))))./(x_ - 1);
end
