% GFUNC_3COMP  Thermodynamic model (Gibbs energy surface) of the
% ASM(1)-UR(2)-H2O(3) ternary system
% 
% Usage:
%   [G, mu1, mu2, mu3] = Gfunc_3comp(x2, x3, T, param)
%
% Pitzer-Simonson model is used inside the function. Value of G is given
% per mol of NOT DISSOCIATED components
%
% See also CALC_APHI, GETR

% Programming: Alexey Voskov
% License: MIT
function [G, mu1, mu2, mu3] = Gfunc_3comp(x2, x3, T, param, verbose)
% Debug output flag initialization
if nargin == 5 && strcmp(verbose, 'verbose')
    verbose = true;
else
    verbose = false;
end
% Molar fractions of the components processing
x1 = 1 - x2 - x3;
x1(x1 < eps) = eps;
x2(x2 < eps) = eps;
x3(x3 < eps) = eps;
R = getR();
% Calculate concentrations of species (neutral and ionic)
xsum = 1 + x1;
xI = x1 ./ xsum;
xUR = x2 ./ xsum;
xW  = x3 ./ xsum;
% Calculate configurational (based on entropy of mixing) part of Gibbs
% energy
Gid = R*T*(2.*xI.*log(2.*xI) + xUR.*log(xUR) + xW.*log(xW));
% Calculate residual part of Gibbs energy
% a) binary parameters (Pitzer-Simonson model)
W_URI = 1.540 - 5.836e-3*T; % UR(2)-ASM(1)
U_URI = 7.7055e-1;
W_WI  =  1.348 - 4.441e-3*T;  % H2O(3)-ASM(1)
U_WI  = -3.062 + 9.356e-3*T;
% b) binary interactions
G12 = 2.*xI.*xUR .* (W_URI + (2/3)*U_URI.*(1 - xUR + xI));
G13 = 2.*xI.*xW  .* (W_WI + (2/3)*U_WI.*(1 - xW + xI));
t = T ./ 300;
W_WUR = -(9.905)./t  + (5.474).*log(T)  - (28.65)*t + (7.119)*t.^2;
U_WUR = -(8.063)./t  + (3.076).*log(T)  - (9.510)*t;
V_WUR =  (26.746)./t - (13.381).*log(T) + (64.313)*t - (14.237)*t.^2;
G23 = xUR.*xW.*(W_WUR + U_WUR.*(xW - xUR) + V_WUR.*(xUR.^2 + xW.^2));
% c) ternary interactions
G123 = 2*xI.*xUR.*xW.*(param(1) + param(2)*T);
% d) PDH interaction
Aphi = calc_Aphi(T);
Ax = Aphi * sqrt(1000 / 18.0153); % 2.918 at 298.15 K
p = 14.9;
Ix = xI;
GPDH = -(4*Ax.*Ix)./p.*log( (1 + p*sqrt(Ix)) ./ (1 + p*sqrt(0.5)) );
% Total Gibbs energy of mixing
Gex = R.*T .* (G13 + G12 + G23 + G123 + GPDH);
G = (Gid + Gex).*xsum;
% Numerical differentiation
if nargout > 1 || verbose
    dx = 1e-6;
    dG_dx2 = (Gfunc_3comp(x2 + dx, x3, T, param) - Gfunc_3comp(x2 - dx, x3, T, param)) ./ (2*dx);
    dG_dx3 = (Gfunc_3comp(x2, x3 + dx, T, param) - Gfunc_3comp(x2, x3 - dx, T, param)) ./ (2*dx);
    mu1 = G - x2.*dG_dx2 - x3.*dG_dx3;
    mu2 = G + (1-x2).*dG_dx2 - x3.*dG_dx3;
    mu3 = G - x2.*dG_dx2 + (1-x3).*dG_dx3;
end
% Debug output and control of derivatives
if verbose
    fprintf('Gfunc_3comp results analysis\n');
    fprintf('  max(Gex / G): %.5f\n', max(abs(Gex ./ (Gex + Gid))));
    fprintf('  max(G - sum(x.*mu)) = %.5e\n', max(abs(G - (mu1.*x1 + mu2.*x2 + mu3.*x3))));
end
end
