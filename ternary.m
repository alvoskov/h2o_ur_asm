% TERNARY  ASM(1)-UR(2)-H2O(3) ternary system optimization. It relies on
% Gibbs energies models of binary subsystems that are hardly encoded
% in Gfunc_3comp.
%
% Warning: h2o_ur, h2o_asm and w_asm will not automatically update
% the content of Gfunc_3comp.
%
% See also Gfunc_3comp, h2o_ur, ur_asm, h2o_asm, compProp,
% lnp0_H2O_kPa, getR

% Programming: Alexey Voskov
% License: MIT
function ternary
save_fig = true;
fprintf('ASM(1)-UR(2)-H2O(3) ternary system\n');
close all;
% Some constants used in the thermodynamic model
R = getR();
T = 298.15;
[~, pH2O_0] = lnp0_H2O_kPa(T); % ~3.1698 kPa

% Liquidus data
data_liq = [
% wUR   wH2O    tliq   teut   wliq
36.23   40.52   -23.5  -29.0  0.2
36.23   40.52   -23.6  -28.9  0.2
18.08   60.26   -15.4  -28.8  1
9.11    79.97   -8.2   -28.9  1
    ];
wUR_liq = data_liq(:,1); 
wH2O_liq = data_liq(:,2);
wASM_liq = 100 - wUR_liq - wH2O_liq;
T_liq = data_liq(:,3) + 273.15;
T_eut = data_liq(:,4) + 273.15;
w_liq = data_liq(:,5); % Statistical weights
[~, xUR_liq, xH2O_liq] = mass_to_x(wASM_liq, wUR_liq, wH2O_liq);

% Vapour pressure data
data = [
%mASM   mUR     mH2O    pH2O, kPa
3.491   5.511   45.237	3.01054
4.884   7.713   20.293	2.72696
5.234   8.262   10.563	2.36178
3.538   1.862   27.376	3.00385
5.896   3.104   13.913	2.69352
8.844   4.656   9.94	2.32700
3.826   0.673   20.435	2.97977
5.894   3.1     13.062	2.72027
11.479  2.015   11.983	2.45274
];
    
mASM = data(:,1);
mUR = data(:,2);
mH2O = data(:,3);
pH2O = data(:,4);

[~, xUR, xH2O] = mass_to_x(mASM, mUR, mH2O);

%param0 = [1000 1 1 1];
param0 = [0 0];
% Test the model function
x1 = 0.4; x2 = 0.35; x3 = 0.25;
[G,mu1,mu2,mu3] = Gfunc_3comp(x2,x3,T,param0);

% Try to optimize ternary parameters
function delta = tern_minfunc(param, chtest)
    % a) eutectic
    Txy = calc_eutectic(param); Teut_calc = Txy(1);
    % b) liquidus curve
    mu3_liq = nan(size(T_liq));
    Teq_liq = nan(size(T_liq));
    for j = 1:numel(mu3_liq)
        Teq_liq(j) = calc_equil_comp3(param, xUR_liq(j), xH2O_liq(j));
    end
    % c) vapour pressure
    [~,~,~,mu3_] = Gfunc_3comp(xUR, xH2O, T, param);
    lnp_ = log(pH2O_0) + mu3_./(R*T);
    % d) delta vector
    pw = ones(size(lnp_));
    pw(8) = 0.5; % Point with large error
    delta = [
        (Teut_calc - T_eut)./T_eut
        (Teq_liq - T_liq)./T_liq.*w_liq
        (exp(lnp_) - pH2O).*pw./pH2O
        ] * 1000;
    
    % Convex hull control
    if nargin == 5
        [X2, X3] = meshgrid(0:0.02:1, 0:0.02:1);
        jj = X2 + X3 <= 1;
        X2 = X2(jj);
        X3 = X3(jj);
        Gm = Gfunc_3comp(X2, X3, 500, param);
        jj = (Gm > 0);
        X2(jj) = [];
        X3(jj) = [];
        Gm(jj) = [];
        k = convhulln([X2 X3 Gm]);
        kv = unique(k);
        if numel(kv) ~= numel(Gm)
            delta = 1e10*ones(size(delta));
        end
    end
end

opt = optimset('FinDiffType', 'central', 'FinDiffRelStep', 1e-3, 'Display', 'iter', 'MaxFunEvals', 2000);
[param,~,res,~,~,~,J] = lsqnonlin(@tern_minfunc, param0, [], [], opt);
f = numel(res) - numel(param);
sigma2 = (res'*res) / f;
sigma2_rounded = sigma2 * 100;
extra_digits = 0;
while abs(sqrt(sigma2_rounded) - sqrt(sigma2)) / sqrt(sigma2) >= 0.0005
    [~,ndigits,param_rounded] = display_parameters(param,res,J,extra_digits);
    res_rounded = tern_minfunc(param_rounded);
    sigma2_rounded = (res_rounded'*res_rounded) ./ f;
    fprintf('sigma2: %.3e\n', sqrt(sigma2_rounded));
    extra_digits = extra_digits + 1;
end
param = param_rounded;
disp('===== ROUNDED PARAMETERS =====');
display_parameters(param_rounded, res_rounded, J, extra_digits);
disp('==============================');

param_ci = nlparci(param,res,'jacobian',J);
dparam = (param_ci(:,2) - param_ci(:,1)) ./ 2;
for i = 1:numel(param)
    fprintf('param(%d)=(%.7e)+-(%.7e)\n', i, param(i), dparam(i));
end

Txy_ = calc_eutectic(param);
fprintf('Ternary eutectic properties:\n');
fprintf('  Teut = %.3f K\n', Txy_(1));
fprintf('  x1 = %.6f (H2O)\n', Txy_(3));
fprintf('  x2 = %.6f (UR)\n', Txy_(2));
fprintf('  x3 = %.6f (ASM)\n', 1 - sum(Txy_(2:3)));

fprintf('Liquidus points:\n');
for i = 1:numel(T_liq)
    fprintf('  x2=%.6f, x3=%.6f, Texp=%.2f, Tcalc=%.2f\n', ...
        xUR_liq(i), xH2O_liq(i), T_liq(i), ...
        calc_equil_comp3(param, xUR_liq(i), xH2O_liq(i)));
end

% Show the information about water vapour pressure
% a) 3D graph without surface (points only)
[~,~,~,mu3] = Gfunc_3comp(xUR, xH2O, T, param, 'verbose');
lnp = log(pH2O_0) + mu3./(R*T);
pH2O_calc = exp(lnp);
plot3(xUR, xH2O, (pH2O - pH2O_calc) ./ pH2O * 100, 'bo', 'LineWidth', 2);
xlabel('{\it{x}}_{(NH_2)_2CO}', 'FontSize', 14);
ylabel('{\it{x}}_{H_2O}', 'FontSize', 14);
zlabel('(p - p^{calc})/p^{calc}, %', 'FontSize',14);
set(gca, 'FontSize', 12);
grid on;

% b) 2D graph
figure;
p_res = (pH2O - pH2O_calc) ./ pH2O;
plot(1:numel(pH2O),  p_res * 100, 'bo', 'LineWidth', 2);
axis([1 numel(pH2O) -2 2]);
xlabel('Points', 'FontSize', 14);
ylabel('$(p - p^\mathrm{calc})/p^\mathrm{calc}$, \%', 'FontSize',14, 'interpreter', 'latex');
set(gca, 'FontSize', 12);
set(gcf, 'Units', 'Centimeters');
set(gcf, 'Position', [0 0 10 8]);
s = sqrt(sumsqr(p_res) ./ numel(p_res)) * 100;
xv = [1 numel(pH2O)];
hold on;
plot(xv, [0 0], 'k-', xv, [s s], 'k--', xv, [-s -s], 'k--', 'LineWidth', 2);
hold off;
set(gcf, 'PaperPositionMode', 'auto');
if save_fig
    save_fig_to_files('Saving 2D graph to files...', 'fig11_dpH2O');
end

% c) 3D graph with surface
[xUR_mat_grid, xH2O_mat_grid] = meshgrid(0:0.01:0.18, 0.75:0.01:1);
pcalc_grid = calc_p_surf(xUR_mat_grid, xH2O_mat_grid, T, param, pH2O_0);
[xUR_mat_surf, xH2O_mat_surf] = meshgrid(0:0.001:0.18, 0.75:0.001:1);
pcalc_surf = calc_p_surf(xUR_mat_surf, xH2O_mat_surf, T, param, pH2O_0);
figure;
surf(xUR_mat_surf, xH2O_mat_surf, pcalc_surf, 'LineStyle', 'none');
hold on;
surf(xUR_mat_grid, xH2O_mat_grid, pcalc_grid);
plot3(xUR, xH2O, pH2O, 'bo', 'LineWidth', 2, 'MarkerFaceColor', 'b');
xlabel('$x_\mathrm{(NH_2)_2CO}$', 'FontSize', 14, 'Interpreter', 'latex');
ylabel('$x_\mathrm{H_2O}$', 'FontSize', 14, 'Interpreter', 'latex');
zlabel('$p$ / kPa', 'FontSize',14, 'Interpreter', 'latex');
set(gca, 'FontSize', 12);
set(gcf, 'Units', 'Centimeters');
set(gcf, 'Position', [0 0 10 8]);
set(gcf, 'PaperPositionMode', 'auto');
grid on;
hold off;
if save_fig
    save_fig_to_files('Saving 3D graph to files...', 'figGA_pH2O_surf', 'bitmap');
end
end

function pcalc = calc_p_surf(xUR_mat, xH2O_mat, T, param, pH2O_0)
R = getR();
[m, n] = size(xUR_mat);
len = numel(xUR_mat);
ii = (xUR_mat + xH2O_mat <= 1);
xUR_mat(~ii) = nan;
xH2O_mat(~ii) = nan;
xUR_mat = reshape(xUR_mat, len, 1);
xH2O_mat = reshape(xH2O_mat, len, 1);

[~,~,~,mu3] = Gfunc_3comp(xUR_mat, xH2O_mat, T, param);
pcalc = exp(real(log(pH2O_0) + mu3./(R*T)));

pcalc = reshape(pcalc, m, n);
end


function [xASM, xUR, xH2O] = mass_to_x(mASM, mUR, mH2O)
persistent MH2O MASM MUR
if isempty(MH2O)
    p = compProp('W'); MH2O = p.M;
    p = compProp('ASM'); MASM = p.M;
    p = compProp('UR'); MUR = p.M;
end

nUR = mUR ./ MUR;
nASM = mASM ./ MASM;
nH2O = mH2O ./ MH2O;
nSUM = nUR + nASM + nH2O;

xASM = nASM ./ nSUM;
xUR = nUR ./ nSUM;
xH2O = nH2O ./ nSUM;
end

function beta = calc_eutectic(param)
    function delta = minfunc(beta)
        T = beta(1); x2 = beta(2); x3 = beta(3);
        [G,mu1,mu2,mu3] = Gfunc_3comp(x2, x3, T, param);
        dmG1 = dmG1_func(T);
        dmG2 = dmG2_func(T);
        dmG3 = dmG3_func(T);
        delta = [dmG1 + mu1, dmG2 + mu2, dmG3 + mu3];
    end
    ASMprop = compProp('ASM');
    URprop  = compProp('UR');
    H2Oprop = compProp('W');
    dmG1_func = ASMprop.dmG_func;
    dmG2_func = URprop.dmG_func;
    dmG3_func = H2Oprop.dmG_func;

    opt = optimset('Display', 'off');
    [beta,~,~,exitflag] = lsqnonlin(@minfunc, [273, 0.33, 0.33], [200, 0, 0], [400, 1, 1], opt);
%    if exitflag <= 0
%        warning('exitflag %d', exitflag);
%    end
    %fprintf('%.5f ', beta); fprintf('|');
    %fprintf('%.5f ', param); fprintf('\n');    
end

function Teq = calc_equil_comp3(param, x2, x3)
    function delta = minfunc1(T)
        [~,mu1,~,~] = Gfunc_3comp(x2, x3, T, param);
        dmG1 = dmG1_func(T);
        delta = abs(dmG1 + mu1);
    end
    function delta = minfunc2(T)
        [~,~,mu2,~] = Gfunc_3comp(x2, x3, T, param);
        dmG2 = dmG2_func(T);
        delta = abs(dmG2 + mu2);
    end
    function delta = minfunc3(T)
        [~,~,~,mu3] = Gfunc_3comp(x2, x3, T, param);
        dmG3 = dmG3_func(T);
        delta = abs(dmG3 + mu3);
    end
    ASMprop = compProp('ASM');
    URprop  = compProp('UR');
    H2Oprop = compProp('W');
    dmG1_func = ASMprop.dmG_func;
    dmG2_func = URprop.dmG_func;
    dmG3_func = H2Oprop.dmG_func;
    Teq = [fminbnd(@minfunc1, 200, 500)
        fminbnd(@minfunc2, 200, 500)
        fminbnd(@minfunc3, 200, 500)];
    %Teq = min(Teq);
    Teq = Teq(3);
end
