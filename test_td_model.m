% TEST_TD_MODEL  Checks the work of TD_MODEL by the next ways:
% 1) Matching of G from direct calculation and from
%    sum of chemical potentials
% 2) Matching of mu1 and mu2 from direct calculation and from
%    numerical calculation (based on G expression numeric
%    differentiation)
% 3) Mean ionic activity coefficients of 1-1 electrolyte fitting
%    data for NH4NO3 and NaCl are supplied in this file. These
%    data are taken from
%      R.A.Robinson and R.H.Stokes "Electrolyte solutions"
%
% See also TD_MODEL

% Programming: Alexey Voskov
% License: MIT
function test_td_model
disp('----- Test 1. Chemical potentials -----');
T = 298.15;
a = [1000 10 1100 20 1200 30];
x = 0.1:0.1:0.9;
a0 = [1111 22 3333 44];    
a_to_g = @(a, T) [a(1) + a(2)*T, a(3) + a(4)*T, 0];    
disp('a) Sum of chemical potentials test');
[mu1, mu2, G, mu1_ex, mu2_ex, G_ex] = td_model(x, T, a, a_to_g);
G_from_mu = (1 - x).*mu1 + x.*mu2;
G_ex_from_mu = (1 - x).*mu1_ex + x.*mu2_ex;
fprintf('max(abs(G - sum(x.*mu))) = %.5e\n', max(abs(G_from_mu - G)));
fprintf('max(abs(Gex - sum(x.*muex))) = %.5e\n', max(abs(G_ex_from_mu - G_ex)));

disp('b) Numerical control of chemical potentials');
dx = 1e-6;
[mu1, mu2, G] = td_model(x, T, a, a_to_g);
[~, ~, G2] = td_model(x + dx, T, a, a_to_g);
[~, ~, G1] = td_model(x - dx, T, a, a_to_g);
dG_dx = (G2 - G1) ./ (2*dx);
mu1_num = G - x.*dG_dx;
mu2_num = G + (1 - x).*dG_dx;
if max(abs(mu1_num - mu1)./abs(mu1)) > 1e-6
    error('mu1 numeric test failed');
else
    fprintf('mu1 numeric test passed\n');
end
if max(abs(mu2_num - mu2)./abs(mu2)) > 1e-6
    error('mu2 numeric test failed');
else
    fprintf('mu2 numeric test passed\n');
end

disp('---- Test 2. Activity coefficients fitting -----');
sys = 'NaCl'; % NH_4NO_3, NaCl
dat = get_sys_data(sys);
Ms = 18.02;
m = dat(:,1);
am = dat(:,2);
phi = dat(:,3);
x = m*Ms ./ (1000 + m*Ms);
T = 298.15;

lnaw = -2*m.*phi*Ms*1e-3;

function lng2 = calc_lngmi(xv, a)
    xi = xv ./ (1 + xv);
    [~,~,~,mu1_ex, mu2_ex] = td_model(xv, T, a, a_to_g);
    lng2 = (mu2_ex ./ 8.3144621 ./ T) / 2;
    lng2 = lng2 - lng2(1) + log(1 - 2*xi);
end

function [lna1, lng1] = calc_lna1(xv, a)
    [mu1,~,~,mu1_ex, mu2_ex] = td_model(xv, T, a, a_to_g);
    lng1 = (mu1_ex ./ 8.3144621 ./ T);
    lna1 = (mu1 ./ 8.3144621 ./ T);
end

a0 = [0 0];    
a_to_g = @(a, T) [a(1), a(2)];
% Nonlinear regression: target function    
function delta = minfunc(a)
    [lna1, lng1] = calc_lna1(x, a);
    lng2 = calc_lngmi(x, a);
    delta = [lng2 - log(am); lna1 - lnaw];
end

% Model parameters fit
a = td_model_fit(a0, @minfunc);
%a = a0;
mv = 0:0.1:6;
xv = mv*Ms ./ (1000 + mv*Ms);

% Derivatives check
dx = 1e-6;
[mu1_calc,mu2_calc,G] = td_model(x, T, a, a_to_g);
[~,~,GB] = td_model(x + dx, T, a, a_to_g);
[~,~,GA] = td_model(x - dx, T, a, a_to_g);
dG_dx = (GB - GA) ./ (2*dx);
mu1_numeric = G - x.*dG_dx;
mu2_numeric = G + (1 - x).*dG_dx;

disp('Derivatives check');
fprintf('mu1 numerical check (max.rel.dev): %.2e\n',...
    max(abs((mu1_numeric(2:end) - mu1_calc(2:end))./mu1_calc(2:end))));
fprintf('mu2 numerical check (max.rel.dev): %.2e\n',...
    max(abs((mu2_numeric(2:end) - mu2_calc(2:end))./mu2_calc(2:end))));

% Show activities
% a) water
close all;
plot(m, exp(lnaw), 'bo', mv, exp(calc_lna1(xv, a)), 'k-', 'LineWidth', 2);
xlabel(sprintf('m(%s)', sys));
ylabel('ln a_w');
figure;
% b) salt
plot(m, exp(log(am)), 'bo', mv, exp(calc_lngmi(xv, a)), 'k-', 'LineWidth', 2);
xlabel(sprintf('m(%s)', sys));
ylabel('\gamma_\pm');
end

function dat = get_sys_data(sys)
switch sys
    case 'NH_4NO_3'
    dat = [ % Activity and osmotic coefficients at 25C
    0.0     1.000   1.000
    0.1     0.740   0.911
    0.2     0.677   0.890
    0.3     0.636   0.876
    0.4     0.606   0.864
    0.5     0.582   0.855
    0.6     0.562   0.847
    0.7     0.545   0.840
    0.8     0.530   0.834
    0.9     0.516   0.829
    1.0     0.504   0.823
    1.2     0.483   0.813
    1.4     0.464   0.803
    1.6     0.447   0.793
    1.8     0.433   0.785
    2.0     0.419   0.776
    2.5     0.391   0.758
    3.0     0.368   0.743
    3.5     0.348   0.728
    4.0     0.331   0.715
    4.5     0.316   0.702
    5.0     0.302   0.690
    5.5     0.290   0.679
    6.0     0.279   0.670
    ];

    case 'NaCl'
    dat = [ % Activity and osmotic coefficient at 25C (NaCl)
    0.0     1.000   1.000
    0.1     0.778   0.932
    0.2     0.735   0.925
    0.3     0.710   0.922
    0.4     0.693   0.920
    0.5     0.681   0.921
    0.6     0.673   0.923
    0.7     0.667   0.926
    0.8     0.662   0.929
    0.9     0.659   0.932
    1.0     0.657   0.936
    1.2     0.654   0.943
    1.4     0.655   0.951
    1.6     0.657   0.962
    1.8     0.662   0.972
    2.0     0.668   0.983
    2.5     0.688   1.013
    3.0     0.714   1.045
    3.5     0.746   1.080
    4.0     0.783   1.116
    4.5     0.826   1.153
    5.0     0.874   1.192
    5.5     0.928   1.231
    6.0     0.986   1.271
    ];
end
end