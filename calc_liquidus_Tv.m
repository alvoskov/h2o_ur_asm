% CALC_LIQUIDUS_TV  Calculates liquidus and eutectic coordinates
% for two-component phase diagram.
%
% Usage:
%   [Tv, x_eut, T_eut] = calc_liquidus_Tv(xv, a, stab_param, a_to_g)
% Inputs:
%   xv -- vector with molar fractions of second component
%   a  -- thermodynamic model parameters
%   stab_param -- struct with dmG1 and dmG2 fields. Each field
%                 contains @(T) (...) anonymous function handle that
%                 should return Gibbs energy of melting of 1st or 2nd
%                 component respectively
%   a_to_g -- @(a,T) (...) anonymous function handle (will be passed
%             to td_model
% Outputs:
%   Tv -- vector with temperatures (calculated from input xv)
%   x_eut, T_eut -- coordinates of the eutectic point
%
% See also: TD_MODEL

% Programming: Alexey Voskov
% License: MIT
function [Tv, x_eut, T_eut] = calc_liquidus_Tv(xv, a, stab_param, a_to_g)
    function d = dmu1(x,T)
        mu1 = td_model(x,T,a,a_to_g);
        d = mu1 + stab_param.dmG1(T);
    end
    function d = dmu2(x,T)
        [~,mu2] = td_model(x,T,a,a_to_g);
        d = mu2 + stab_param.dmG2(T);
    end
    % Calculate the eutectic point
    function delta = feut(xT)
        T = xT(2);
        [mu1,mu2] = td_model(xT(1),T,a, a_to_g);        
        delta = [mu1 + stab_param.dmG1(T); ...
            mu2 + stab_param.dmG2(T)];
    end
    xT = fsolve(@feut, [0.5, 300], optimset('Display', 'off'));
    x_eut = xT(1); T_eut = xT(2);
    % Calculate the liquidus
    Tv = nan(size(xv));
    for i = 1:numel(xv)
        xi = xv(i);
        if xi < x_eut
            Tv(i) = fminbnd(@(T) (dmu1(xi,T)).^2, 200, 500);
        else
            Tv(i) = fminbnd(@(T) (dmu2(xi,T)).^2, 200, 500);
        end            
    end
end